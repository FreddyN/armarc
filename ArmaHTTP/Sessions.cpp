/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <queue>
#include "Sessions.h"

using std::queue;
using std::time;
using std::string;
using std::map;
using std::pair;

const size_t Sessions::_maxsessions = 1000;

Sessions::Sessions()
{
}

Sessions::~Sessions()
{
	_mutex.lock();
	_mutex.unlock();
}

Sessions::Sessions(Sessions& src)
{
	_mutex.lock();
	src._mutex.lock();
	src._cleanGarbage();
	_id_withsessions = src._id_withsessions;
	src._mutex.unlock();
	_mutex.unlock();
}

bool Sessions::addSession(const SessionID& session)
{
	if (!session.ID().length())
		return false;

	_mutex.lock();
	_cleanGarbage();

	if (_id_withsessions.size() > _maxsessions)
	{
		_mutex.unlock();
		return false;
	}

	const auto iSession = _id_withsessions.find(session.ID());

	if (iSession != _id_withsessions.end()) 
	{		
		_mutex.unlock();
		return false;
	}

	_id_withsessions.insert(pair< string, SessionID>(session.ID(), session));
	_mutex.unlock();
	return true;
}

bool Sessions::getSession(const string& cstrID, SessionID& session)
{
	_mutex.lock();
	_cleanGarbage();
	auto iSession
		= _id_withsessions.find(cstrID);

	if (iSession == _id_withsessions.end()) {
		_mutex.unlock();
		return false;
	}
	session = (*iSession).second;
	// ToDo : Seems to be active, increase TTL ?
	_mutex.unlock();
	return true;
}

void Sessions::clearSession(const string& cstrID)
{
	_mutex.lock();
	map< string, SessionID>::iterator iSession
		= _id_withsessions.find(cstrID);

	if (iSession != _id_withsessions.end()) 
	{
		_id_withsessions.erase(iSession);
		_cleanGarbage();
		_mutex.unlock();
		return;
	}
	_cleanGarbage();
	_mutex.unlock();
	return;
}


void Sessions::_cleanGarbage()
{
	auto currentTime = time(nullptr);
	queue< map< string, SessionID>::iterator > cleanup;

	auto iBegin = _id_withsessions.begin();
	auto iEnd = _id_withsessions.end();
	while (iBegin != iEnd)
	{
		if (iBegin->second.getLifeTime() < currentTime)
			cleanup.push(iBegin);
		++iBegin;
	}
	
	while (!cleanup.empty())
	{		 
		_id_withsessions.erase(cleanup.front());
		cleanup.pop();
	}	
}

size_t Sessions::size()
{
	_mutex.lock();
	auto result = _id_withsessions.size();
	_mutex.unlock();
	return result;
}

const map< string, SessionID>& Sessions::getMap() const
{
	return _id_withsessions;
}