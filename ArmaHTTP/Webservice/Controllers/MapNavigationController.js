/**
 * Created by Simon on 25.07.2015.
 */

armaGpsApp.controller('MainNavigationController', ['$scope', '$http', '$interval', function ($scope, $http, $interval) {

    /*
     please insert your server configuration here
     */
    $scope.serverConfig = {
        "port": 9192,
        "host": "./",
        "protocol": 'http://'
    };

    $scope.scaleFactor = 2;

    $scope.mapConfiguration = undefined;
    $scope.pause = true;
    $scope.gameInterval = null;
    $scope.mapInterval = null;

    $scope.welcomeMessage = 'Welcome to : ';
    $scope.armaJson = new Array();
    $scope.errorMessage = null;

    /**
     * binding the scroll event
     * @param $event
     * @param delta
     * @param deltax
     * @param deltay
     */
    $scope.scroll = function($event, delta, deltax, deltay) {
        console.log($event, delta, deltax, deltay);
    };

    /**
     * this functions polls new data from the webserver
     * to be sure that the data is not corrupt please
     * DO NOT poll faster than every 1/4 sec.
     */
    $scope.getAllPlayerData = function () {
        var url = '/api/getct?key=AllPlayers';
        $http.get(url).
            success(function (data) {
                $scope.clearPlayerData();
                for (var i = 0; i < data.names.length; i++) {
                    $scope.createPlayerDataFromJson(
                        data.names[i],
                        data.positions[i],
                        data.alive[i],
                        data.dir[i],
                        data.side[i],
                        data.speed[i],
                        data.tickcount
                    );

                }
                $scope.errorMessage = null;
            }).
            error(function (data, status) {
                $scope.errorMessage = data + status;
                return;
            });
        console.log("new data received");
    };

    /**
     * this functions loads initially the map configuration
     */
    $scope.getMapConfiguration = function () {
        var url = '/api/getct?key=MapConf';
        $http.get(url).
            success(function (data) {
                $scope.mapConfiguration = data;
                console.log("Map conf: " + $scope.mapConfiguration.width);
            }).
            error(function (data, status) {
                return;
            });

    };

    /**
     * create a player object from data
     * @param playerName
     * @param positions
     * @param isAlive
     * @param direction
     * @param team
     * @param speed
     * @param tickCount
     */
    $scope.createPlayerDataFromJson = function (playerName, positions, isAlive, direction, team, speed, tickCount) {

       /* create new player object from the params */
        var newPlayerData = {
            "name": playerName,
            "position": {
                "x": positions[0],
                "y": positions[1],
                "z": positions[2]
            },
            "isAlive": isAlive,
            "direction": direction,
            "team": team,
            "speed": speed,
            "tickCount": tickCount
        };
        $scope.armaJson.push(newPlayerData);
    };

    /**
     * this void function clears all received player data
     */
    $scope.clearPlayerData = function () {
        $scope.armaJson = [];
    };


    /**
     * creates a marker object
     * @param type
     * @param size
     * @param posX
     * @param posY
     * @param direction
     * @param text
     * @returns {{type: *, size: *, x: *, y: *, direction: *, text: *}}
     */
    $scope.createMarker = function (type, size, posX, posY, direction, text) {
        var newMarker = {
            "type": type,
            "size": size,
            "x": posX,
            "y": posY,
            "direction": direction,
            "text": text
        };
        return newMarker;
    };

    /**
     * send marker data to server
     */
    $scope.sendMarker = function (marker) {

    };

    /**
     * pause the polling for data
     */
    $scope.togglePause = function () {
        if ($scope.pause == true) {
            $scope.pause = false;
            console.log("receiving data ...");
            $scope.gameInterval = setInterval($scope.getAllPlayerData, 250);
            $scope.mapInterval = setInterval($scope.getMapConfiguration, 10000);
        } else {
            clearInterval($scope.gameInterval);
            clearInterval($scope.mapInterval);
            console.log("Stop receiving data ...");
            $scope.pause = true;
        }
    };

    /**
     * init function
     */
    $scope.init = function() {
        $scope.getMapConfiguration();
        $scope.togglePause();


    };


    /**
     * this functions is used to translate a position relative
     * to the scaleFactor
     * @param posX
     * @returns {number}
     */
    $scope.translatePlayerPositionX = function( posX ) {
        return posX / $scope.scaleFactor;
    };

    /**
     * this function is used to translate a position relative
     * to the scaleFactor
     * @param posY
     * @returns {number}
     */
    $scope.translatePlayerPositionY = function(  posY ) {
        return ($scope.mapConfiguration.height - posY) / $scope.scaleFactor;

    };

    $scope.init();
}]);