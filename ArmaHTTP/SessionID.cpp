/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <string>
#include "SessionID.h"

using std::string;
using std::time;

SessionID::SessionID(const string& cstrSID, const string& cstrUID, time_t timetolive, trustLevel tl, bool certified, bool deflate):
	_sessionID(cstrSID),
	_UID(cstrUID),
	_timetolive(timetolive),
	_trustlevel(tl),
	_certified(certified),
	_supportDeflate(deflate)
{
}

SessionID::SessionID() :
	_sessionID(""),
	_UID(""),
	_timetolive(0),
	_trustlevel(untrusted),
	_certified(false),
	_supportDeflate(false)
{

}

SessionID::SessionID(const string& cstrUID, int timetolive, trustLevel tl, bool deflate) :
		_sessionID(""),
		_UID(cstrUID),
		_trustlevel(tl),
		_certified(false)
{
	_timetolive = time(nullptr);
	_timetolive += timetolive;
	_supportDeflate = deflate;
	_createASessionID();
}


SessionID::SessionID(const SessionID& src)
{
	_timetolive = src._timetolive;
	_UID = src._UID;
	_sessionID = src._sessionID;
	_trustlevel = src._trustlevel;
	_certified = src._certified;
	_supportDeflate = src._supportDeflate;
}

SessionID::~SessionID()
{
};

const string& SessionID::UID() const
{
	return _UID;
}

const string& SessionID::ID() const
{
	return _sessionID;
}

const time_t& SessionID::getLifeTime() const
{
	return _timetolive;
}

void SessionID::setTrustLevel(trustLevel val)
{
	_trustlevel = val;
}

void SessionID::setDeflate(bool deflate)
{
	_supportDeflate = deflate;
}

bool SessionID::isSupportingDeflate() const
{
	return _supportDeflate;
}

SessionID::trustLevel SessionID::getTrustLevel() const
{
	return _trustlevel;
}

void SessionID::_createASessionID()
{
	_sessionID = "SID";
	static const size_t IDSize = 16;
	static const string cstrAlphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static const size_t Alphabetsize = string(cstrAlphabet).length() - 1;
	while (_sessionID.length() < IDSize)
	{
		const auto index = (rand() * Alphabetsize) / RAND_MAX;
		if ( index < Alphabetsize )
			_sessionID += cstrAlphabet[index];
	}
}

void SessionID::setCertified(bool val)
{
	_certified = val;
}

bool SessionID::isCertified() const
{
	return _certified;
}


