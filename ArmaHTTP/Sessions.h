/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_Sessions
#define _H_Sessions
#include <mutex>
#include <map>
#include <string>
#include <set>
#include "SessionID.h"

using std::mutex;
using std::map;
using std::string;
using std::set;

class Sessions
{
public:
	Sessions();
	Sessions( Sessions& src);
	~Sessions();

	bool addSession(const SessionID& session);
	bool getSession(const string& cstrID, SessionID& session);
	void clearSession(const string& cstrID);
	const map< string, SessionID>& getMap() const;
	size_t size();
	
protected:
	static const size_t _maxsessions;
	void _cleanGarbage();

protected:
	mutex					_mutex;
	map< string, SessionID> _id_withsessions;
};

#endif
