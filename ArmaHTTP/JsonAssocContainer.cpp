/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "JsonAssocContainer.h"
#include <json/json.h>

using std::pair;
using std::string;

// Recursively copy the values of src into dest. Both src and dest must be objects.
static void _mergeJsonVal(Json::Value& dest, Json::Value src)
{
	if (!src.isObject() || !dest.isObject()) 
		return;
	for (const auto& key : src.getMemberNames()) 
	{
		if (src[key].isObject())
			_mergeJsonVal(dest[key], src[key]);		
		else
			dest[key] = src[key];		
	}
}

// Recursively copy the values of src into dest. Both src and dest must be objects.
static void _removeFromJson(Json::Value& dest, const Json::Value src, const string& strKeyToRemove, const string& strPath = "")
{
	if (!src.isObject())
		return;
	for (const auto& key : src.getMemberNames())
	{
		if (src[key].isObject())
		{
			string strNewPath = strPath + key + "/";
			_removeFromJson(dest[key], src[key], strKeyToRemove, strNewPath );
		}
		else
		{
			string currentPath = strPath + key;
			if ( currentPath.compare( strKeyToRemove) != 0 )
				dest[key] = src[key];
		}
	}
}

const size_t JsonAssocContainer::_maxItems = 1000;

JsonAssocContainer::JsonAssocContainer(bool validJson ):
	_validJson( validJson )
{
}

JsonAssocContainer::JsonAssocContainer( JsonAssocContainer& src)
{
	_mutex.lock();
	src._mutex.lock();
	_cache = src._cache;
	_protected = src._protected;
	_validJson = src._validJson;
	src._mutex.unlock();
	_mutex.unlock();
}


JsonAssocContainer::~JsonAssocContainer()
{
	_mutex.lock();
	_mutex.unlock();
}

bool JsonAssocContainer::set(const string& key, const string& val, bool writeprotected)
{
	if (_validJson)
	{
		Json::Reader	jsr;
		Json::Value		root;
		if (!jsr.parse(val, root, false))
			return false;
		// Json is valid
	}

	_mutex.lock();
	auto iWriteProtected = _protected.find(key);
	if (iWriteProtected != _protected.end())
	{
		_mutex.unlock();
		return false;
	}

	if (_cache.find(key) == _cache.end())
	{
		if ( _cache.size() < _maxItems )
			_cache.insert(pair<string, string>(key, val));
		else
		{
			_mutex.unlock();
			return false;
		}
	}
	else
		_cache[key] = val;
	if (writeprotected)
		_protected.insert(pair<string, bool>(key, true));
	_mutex.unlock();
	return true;
}

const char* JsonAssocContainer::isKey(const string& key)
{
	const char* result = nullptr;
	_mutex.lock();
	map<string, string>::const_iterator iFind = _cache.find(key);
	if (iFind == _cache.end())
	{
		_mutex.unlock();
		return result;
	}
	result = (*iFind).second.c_str();
	_mutex.unlock();
	return result;
}

bool JsonAssocContainer::merge(const string& key, const string& val)
{
	auto valDest = isKey(key);
	_mutex.lock();
	auto iWriteProtected = _protected.find(key);
	if (iWriteProtected != _protected.end())
	{
		_mutex.unlock();
		return false;
	}
	_mutex.unlock();

	if (!valDest)
		return set(key, val);		

	Json::Reader	jsr;
	Json::Value		rootSrc;
	if (!jsr.parse(val, rootSrc, false))
		return false;

	Json::Value		rootDest;
	jsr.parse(valDest, rootDest, false);

	_mergeJsonVal(rootDest, rootSrc);
	Json::FastWriter writer;
	set(key, writer.write(rootDest));
	return true;
}

bool JsonAssocContainer::getCopy(const string& key, string& val)
{	
	_mutex.lock();
	map<string, string>::const_iterator iFind = _cache.find(key);
	if (iFind == _cache.end())
	{
		_mutex.unlock();
		return false;
	}
	val = (*iFind).second.c_str();
	_mutex.unlock();
	return true;
}

bool JsonAssocContainer::getValue(const string& key, const string& strPath, string& valResult)
{	
	_mutex.lock();
	if (!strPath.length())
	{
		_mutex.lock();
		return false;
	}
	
	map<string, string>::const_iterator iFind = _cache.find(key);
	if (iFind == _cache.end())
	{
		_mutex.unlock();
		return false;
	}

	Json::Reader	jsr;
	Json::Value		rootSrc;
	if (!jsr.parse((*iFind).second.c_str(), rootSrc, false))
	{
		_mutex.unlock();
		return false;
	}

	auto jsVal = rootSrc;
	auto& jsPrev = jsVal;
	string strPrevKey;
	auto strAuxPath = strPath;
	auto posSeparator = strAuxPath.find_first_of("/");
	strPrevKey = strAuxPath.substr(0, posSeparator);
	if (posSeparator != string::npos)
	{
		do
		{
			jsPrev = jsVal;
			jsVal = jsVal[strPrevKey];
			// Go deeper and follow the path
			strAuxPath = strAuxPath.substr(posSeparator + 1);
			posSeparator = strAuxPath.find_first_of("/");
			strPrevKey = strAuxPath.substr(0, posSeparator);
		} while ((posSeparator != string::npos) && (jsVal.isObject()));
		jsPrev = jsVal;
		jsVal = jsVal[strAuxPath];
	}
	else
	{
		jsPrev = jsVal;
		jsVal = jsVal[strAuxPath];
	}
	

	if ((jsVal.isNull()) || (jsVal.isObject()))
	{
		valResult = "[]";
		_mutex.unlock();
		return false;
	}

	if (jsVal.isArray())
	{
		Json::FastWriter writer;
		valResult = writer.write(jsVal);
		valResult.pop_back();
	}
	else
	{
		Json::Value root;
		root.append(jsVal);
		Json::FastWriter writer;
		valResult = writer.write(root);
		valResult.pop_back();
	}
	_mutex.unlock();
	return true;
}

bool JsonAssocContainer::fetchValue(const string& key, const string& strPath, string& valResult )
{
	_mutex.lock();
	if (!strPath.length())
	{
		_mutex.unlock();
		return false;
	}

	auto iWriteProtected = _protected.find(key);
	if (iWriteProtected != _protected.end())
	{
		_mutex.unlock();
		return false;
	}

	map<string, string>::const_iterator iFind = _cache.find(key);
	if (iFind == _cache.end())
	{
		_mutex.unlock();
		return false;
	}

	Json::Reader	jsr;
	Json::Value		rootSrc;
	if (!jsr.parse((*iFind).second.c_str(), rootSrc, false))
	{
		_mutex.unlock();
		return false;
	}

	auto jsVal = rootSrc;
	auto& jsPrev = jsVal;
	string strPrevKey;
	auto strAuxPath = strPath;
	auto posSeparator = strAuxPath.find_first_of("/");
	strPrevKey = strAuxPath.substr(0, posSeparator);
	if (posSeparator != string::npos)
	{ 
		do
		{
			jsPrev = jsVal;
			jsVal = jsVal[strPrevKey];
			// Go deeper and follow the path
			strAuxPath = strAuxPath.substr(posSeparator + 1);
			posSeparator = strAuxPath.find_first_of("/");
			strPrevKey = strAuxPath.substr(0, posSeparator);
		} while ((posSeparator != string::npos) && (jsVal.isObject()));
		jsPrev = jsVal;
		jsVal = jsVal[strAuxPath];

	}
	else
	{
		jsPrev = jsVal;
		jsVal = jsVal[strAuxPath];
	}


	if ( (jsVal.isNull()) || (jsVal.isObject()) )
	{
		_mutex.unlock();
		valResult = "[]";
		return false;
	}


	if (jsVal.isArray())
	{
		Json::FastWriter writer;
		valResult = writer.write(jsVal);
		valResult.pop_back();
	}
	else
	{
		Json::Value root;
		root.append(jsVal);
		Json::FastWriter writer;
		valResult = writer.write(root);
		valResult.pop_back();
	}	

	Json::Value newJSContainer;
	_removeFromJson(newJSContainer, rootSrc, strPath);
	Json::FastWriter writer;
	auto newJsonString = writer.write(newJSContainer);
	_cache[key] = newJsonString;
	_mutex.unlock();
	return true;
}