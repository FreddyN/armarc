/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_JsonAssocContainer
#define _H_JsonAssocContainer
#include <map>
#include <mutex>

using std::map;
using std::string;
using std::mutex;
class JsonAssocContainer
{
public:
	JsonAssocContainer(bool JustValidatJson = false );
	JsonAssocContainer( JsonAssocContainer& src);
	~JsonAssocContainer();
	bool set(const string& key, const string& val, bool writeprotected = false );
	bool merge(const string& key, const string& val);
	const char* isKey(const string& key);
	bool getCopy(const string& key, string& val);
	bool fetchValue(const string& key, const string& strPath, string& valResult);
	bool getValue(const string& key, const string& strPath, string& valResul);
protected:
	map< string, string >	_cache;
	map< string, bool >		_protected;
	mutex					_mutex;
	bool					_validJson;	
	const static size_t		_maxItems;
};
#endif
