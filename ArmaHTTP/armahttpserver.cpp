/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <Windows.h>
#include <string>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <atomic>
#include <thread>
#include <iomanip>
#include <json/json.h>
#include "mongoose.h"
#include "JsonAssocContainer.h"
#include "StdLogger.h"
#include "ModuleLoggerDecorator.h"

#include "Sessions.h"
#include "Sessions2Json.h"
#include "HttpHeader.h"
#include "t_to_string.h"
#include "zlib.h"

using std::string;
using std::stringstream;

using std::atomic;
using std::thread;

using std::vector;
using std::map;

using std::time;
using std::put_time;
using std::gmtime;

using std::runtime_error;
using std::exception;

using std::ifstream;
using std::ios;

using std::find;
using std::transform;

// Some defaults
extern StdLogger stdlog;
int	g_httpPort = 9192;
int g_SessionLiveTime = 7 * 24 * 60 * 60;
static const string g_strapi = "/api/";
static const TCHAR* g_ProductSpecificPath = _T("\\Banana Squad\\ArmaHTTP\\");

// By default protected JSON Containers, can be overwritten with ArmaRCConfig.json
static const char g_JSQFArmaRCConfig[] = "ArmaRC";
static const char g_JSQFSessions[] = "SIDs";
static string     g_HTTPDocumentRoot = "";
vector< string > g_httpprotected_keys = { g_JSQFArmaRCConfig , g_JSQFSessions };

// Some very ugly ifdef's, but it's just for gaming ^^, and just data is changing, no code change 
#ifdef _WIN32
	inline void usleep(size_t mseconds)
	{
		Sleep(mseconds);
	}

	static const string g_PathSeparator = "\\";

	string getCommonAppFolder()
	{
		TCHAR szPath[MAX_PATH];
		if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_COMMON_APPDATA, NULL, 0, szPath)))
		{
			// Append product-specific path
			PathAppend(szPath, g_ProductSpecificPath );
			return string(szPath);
		}
		else
			return string(".\\");
	}

	string getHTTPSessionsFile()
	{
		return getCommonAppFolder() + ".Sessions.json";
	}

	string getHTTPConfigFile()
	{
		return getCommonAppFolder() + "ArmaRCConfig.json";
	}

	string getHTTPRootFolder()
	{
		if (g_HTTPDocumentRoot.length())
			return g_HTTPDocumentRoot;
		static const string g_strhtmlroot = "./Webservice";
		return getCommonAppFolder() + g_strhtmlroot;
	}

	bool createComomAppFolder()
	{
		int result = SHCreateDirectoryEx(NULL, getCommonAppFolder().c_str(), NULL);
		if ((result == ERROR_ALREADY_EXISTS) || (result == ERROR_SUCCESS))
			return true;
		return false;
	}

#else
	static const std::string g_PathSeparator = "/";

	string getCommonAppFolder()
	{
		return std::string("./");
	}

	bool createComomAppFolder()
	{
		mkdir(getCommonAppFolder().c_str());
	}
#endif


#ifdef _DEBUG
	// Checking valid JSON syntax 
	JsonAssocContainer g_jsonAssocContainer( true );
	map<string, JsonAssocContainer> g_userlocal_JsonAssocContainer;
#else
	// Skip JSON validation and "trust" incoming data, it's faster
	JsonAssocContainer g_jsonAssocContainer( false );
	map< string, JsonAssocContainer> g_userlocal_JsonAssocContainer;
#endif

volatile bool g_exitServer = false;
atomic<bool> g_ServerRunning(false);
Sessions g_HTTPSessions;


SessionID lookupSessionID(Sessions& s, struct mg_connection *conn)
{
	// loopup "sessionid" in Cookies
	SessionID result;
	const size_t numHeaders = static_cast<size_t>(conn->num_headers);
	for (size_t i = 0; i < numHeaders; i++)
	{
		mg_connection::mg_header& header = conn->http_headers[i];		
		if (string("Cookie").compare(header.name) == 0)
		{			
			const string& val = header.value;
			const size_t keyVal_separator = val.find_first_of("=");
			if (keyVal_separator != string::npos)
			{
				const string& cookie_key = val.substr(0, keyVal_separator);				
				const string& cookie_val = val.substr(keyVal_separator + 1);				
				if (cookie_key.compare("sessionid") == 0)
				{
					s.getSession(cookie_val, result);
					return result;
				}
			}
		}
	}
	return result;
}

bool lookupDeflateCompression(struct mg_connection *conn)
{
	bool result = false;
	bool notInternetExplorer = false;
	const size_t numHeaders = static_cast<size_t>(conn->num_headers);
	for (size_t i = 0; i < numHeaders; i++)
	{
		mg_connection::mg_header& header = conn->http_headers[i];
		if (string("Accept-Encoding").compare(header.name) == 0)
		{
			const string& val = header.value;
			if (val.find("deflate") != string::npos)
				result = true;
		}
		else
		{
			if (string("User-Agent").compare(header.name) == 0)
			{
				const string& val = header.value;
				size_t pos = val.find("Chrome");
				if (pos != string::npos)
					notInternetExplorer = true;
			}
		}
	}
	return (result && notInternetExplorer);
}

void showSession(ILogger& log, int ll, const SessionID& session)
{
	stringstream strMsg;
	strMsg
		<< "SID:" << session.ID()
		<< " UID:" << session.UID()
		<< " TrustLevel:" << session.getTrustLevel()
		<< " Certified:" << session.isCertified();
	log.log(ll, strMsg.str());
}

string getGMTDate(time_t secoffset = 0L)
{
	auto t = time(nullptr) + secoffset;
	stringstream strout;
	strout << put_time(gmtime(&t), "%a, %d-%b-%y %X GMT");
	return strout.str();
}

string setGMTDate(const time_t& time)
{	
	stringstream strout;
	strout << put_time(gmtime(&time), "%a, %d-%b-%y %X GMT");
	return strout.str();
}

void pushSessions2JsonContainer(const string& keyName, Sessions& src, JsonAssocContainer& dest)
{
	if (src.size() > 0)
	{
		Json::Value root;
		Sessions2JSQF(src, root);
		Json::FastWriter writer;
		dest.set(keyName, writer.write(root), false);
	}
	else
		dest.set(keyName, "{ \"sessions\": [] }" );
}

static void SendHTTPError(struct mg_connection* conn, HttpHeader::statuscode sc, const string& errorInfo, ILogger* pLog = NULL )
{
	stringstream strErrorInfo;
	strErrorInfo << sc.val << " " << sc.text << ":" << errorInfo;
	auto strAux = strErrorInfo.str();

	string strHeader = HttpHeader(sc, vector<HttpHeader::keyVal>({
		HttpHeader::keyVal(HttpHeader::pdhContentType.key, []() { return string("text/plain"); }),
		HttpHeader::keyVal(HttpHeader::pdhContentLength.key, [&strAux]() { return t_to_string<size_t>(strAux.length()); })
	}
	)).toString();
	
	mg_write(conn, strHeader.c_str(), strHeader.length());
	mg_write(conn, strAux.c_str(), strAux.length());
	conn->status_code = sc.val;
	if (pLog)
	{
		string strLogErrorReply = "Send HTTP Error:";
		strLogErrorReply.append(strAux);
		pLog->log(ILogger::logLevel::verbose, strLogErrorReply);
	}
}
 
static mg_result handle_restful_call_getct(struct mg_connection *conn)
{
	ModuleLoggerDecorator mlog(__func__, &stdlog);
	auto session = lookupSessionID(g_HTTPSessions, conn);
	
	if (mlog.isLogged(ILogger::logLevel::verbose))
		showSession(mlog, ILogger::logLevel::verbose, session);

	char strKey[256];
	const auto getvar_result = mg_get_var(conn, "key", strKey, sizeof(strKey));
	if (getvar_result < 0)
	{
		SendHTTPError(conn, HttpHeader::sc_BadRequest, "Missing parameter 'key'", &mlog);
		return MG_FALSE;
	}

	const string cstrKey = strKey;
	if (session.getTrustLevel() < SessionID::trustLevel::uid_authenticated_admin)
	{
		// Just accessable for Admins
		if (find(g_httpprotected_keys.begin(), g_httpprotected_keys.end(), cstrKey) != g_httpprotected_keys.end())
		{
			SendHTTPError(conn, HttpHeader::sc_Forbidden, "'key' exist, but protected for HTTP.", &mlog);
			return MG_FALSE;
		}
	}

	string strJSONContainer;
	if (!g_jsonAssocContainer.getCopy(string(strKey), strJSONContainer))
	{
		string strMsg = strKey;
		strMsg.append(" not exist/not set yet.");
		SendHTTPError(conn, HttpHeader::sc_BadRequest, strMsg, &mlog);
		return MG_FALSE;
	}

	if (session.isSupportingDeflate())
	{
		auto compressBufferSize = compressBound(strJSONContainer.length());
		auto compressBuffer = new char[compressBufferSize];
		if (compress((Bytef*)compressBuffer, &compressBufferSize, (const Bytef*)strJSONContainer.c_str(), strJSONContainer.length()) == Z_OK)
		{

			string strHeader = HttpHeader(HttpHeader::sc_OK,
				vector<HttpHeader::keyVal>({
				HttpHeader::keyVal(HttpHeader::pdhCacheControl.key, []() { return string("no-cache, no-store, must-revalidate"); }),
				HttpHeader::keyVal(HttpHeader::pdhPragma.key, []() { return string("no-cache"); }),
				HttpHeader::keyVal(HttpHeader::pdhConnectionKeepAlive),
				HttpHeader::keyVal(HttpHeader::pdhContentType.key, []() { return string("application/json"); }),
				HttpHeader::keyVal(HttpHeader::pdhContentLength.key, [&compressBufferSize]() { return t_to_string < size_t >(compressBufferSize); }),
				HttpHeader::keyVal(HttpHeader::pdhContentEncodingDeflate)
			})).toString();
			mg_write(conn, strHeader.c_str(), strHeader.length());
			mg_write(conn, compressBuffer, compressBufferSize);
			delete[] compressBuffer;
			return MG_TRUE;
		}
		delete[] compressBuffer;
	}

	string strHeader = HttpHeader(HttpHeader::sc_OK,
		vector<HttpHeader::keyVal>({
		HttpHeader::keyVal(HttpHeader::pdhCacheControl.key, []() { return string("no-cache, no-store, must-revalidate"); }),
		HttpHeader::keyVal(HttpHeader::pdhPragma.key, []() { return string("no-cache"); }),
		HttpHeader::keyVal(HttpHeader::pdhConnectionKeepAlive),
		HttpHeader::keyVal(HttpHeader::pdhContentType.key, []() { return string("application/json"); }),
		HttpHeader::keyVal(HttpHeader::pdhContentLength.key, [&strJSONContainer]() { return t_to_string < size_t >(strJSONContainer.length()); })
	})).toString();

	mg_write(conn, strHeader.c_str(), strHeader.length());
	mg_write(conn, strJSONContainer.c_str(), strJSONContainer.length());
	return MG_TRUE;
}


static mg_result handle_restful_call_setct(struct mg_connection *conn)
{
	ModuleLoggerDecorator mlog(__func__, &stdlog);
	auto session = lookupSessionID(g_HTTPSessions, conn);
	if (mlog.isLogged(ILogger::logLevel::verbose))
		showSession(mlog, ILogger::logLevel::verbose, session);

	if (!session.ID().length())
	{
		SendHTTPError(conn, HttpHeader::sc_Forbidden, "No session ID, not logged in.", &mlog);
		return MG_FALSE;
	}
	else
	{
		if (mlog.isLogged(ILogger::logLevel::verbose))
		{
			stringstream strMsg;
			strMsg << "Session ID:" << session.ID() << " UID:" << session.UID() << " trustlevel:" << session.getTrustLevel();
			mlog.log(ILogger::logLevel::verbose, strMsg.str());
		}
	}

	char strKey[256];
	const auto getvar_result = mg_get_var(conn, "key", strKey, sizeof(strKey));
	if (getvar_result < 0)
	{
		SendHTTPError(conn, HttpHeader::sc_BadRequest, "Missing parameter 'key'", &mlog);
		return MG_FALSE;
	}
	
	const string cstrKey = strKey;
	
	if (session.getTrustLevel() < SessionID::trustLevel::uid_authenticated_admin)
	{
		if (find(g_httpprotected_keys.begin(), g_httpprotected_keys.end(), cstrKey) != g_httpprotected_keys.end())
		{
			SendHTTPError(conn, HttpHeader::sc_Forbidden, "'key' exist, but protected for HTTP.", &mlog);
			return MG_FALSE;
		}
	}

	auto reqMethod = conn->request_method;
	const string& content = conn->content;
	if (!g_jsonAssocContainer.set(string(strKey), content))
	{
		SendHTTPError(conn, HttpHeader::sc_BadRequest, "Failed. Maybe write protected.", &mlog);
		return MG_FALSE;
	}

	auto strResult = HttpHeader(HttpHeader::sc_OK).toString();	
	mg_write(conn, strResult.c_str(), strResult.length());	
	return MG_TRUE;
}

static mg_result handle_restful_call_mergect(struct mg_connection *conn)
{
	ModuleLoggerDecorator mlog(__func__, &stdlog);

	auto session = lookupSessionID(g_HTTPSessions, conn);
	if (mlog.isLogged(ILogger::logLevel::verbose))
		showSession(mlog, ILogger::logLevel::verbose, session);

	if (!session.ID().length())
	{
		SendHTTPError(conn, HttpHeader::sc_Forbidden, "No session ID, not logged in.", &mlog);
		return MG_FALSE;
	}		

	char strKey[256];
	const auto getvar_result = mg_get_var(conn, "key", strKey, sizeof(strKey));
	if (getvar_result < 0)
	{
		SendHTTPError(conn, HttpHeader::sc_BadRequest, "Missing parameter 'key'", &mlog);
		return MG_FALSE;
	}

	auto reqMethod = conn->request_method;
	const string& content = conn->content;

	if (!g_jsonAssocContainer.merge(string(strKey), content))
	{
		SendHTTPError(conn, HttpHeader::sc_BadRequest, "Failed. Maybe write protected", &mlog);
		return MG_FALSE;	
	}

	auto strResult = HttpHeader(HttpHeader::sc_OK).toString();
	mg_write(conn, strResult.c_str(), strResult.length());
	return MG_TRUE;
}


bool CreateSessionID(const string& cstrUID, SessionID& sessionID, bool deflate)
{
	static const char strAdminUIDs[] = "adminUIDs";
	static const char strTrustedUIDs[] = "trustedUIDs";
	static const char strGuestUIDs[] = "guestUIDs";
	static const char strBannedUIDs[] = "bannedUIDs";
	ModuleLoggerDecorator mlog(__func__, &stdlog);
	string strArmaRC;
	if (!g_jsonAssocContainer.getCopy("ArmaRC", strArmaRC))
		return false;
	
	// No Default Player UID
	if (cstrUID.compare("_SP_PLAYER_") == 0)
		return false;

	Json::Value root;
	Json::Reader jsr;
	if (!jsr.parse(strArmaRC, root))
	{
		mlog.log(ILogger::logLevel::error, "ArmaRC parser error");
		return false;
	}
	// Check Banned UIDs
	if (root[strBannedUIDs].isArray())
	{
		const auto& bannedUID = root[strBannedUIDs];
		if (bannedUID.size() > 0)
		{
			for (Json::ArrayIndex i = 0; i < bannedUID.size(); i++)
			{
				if (bannedUID[i].asString().compare(cstrUID) == 0)
				{
					stringstream strMsg;
					strMsg << cstrUID << " is kickbanned. Session ID not assigned";
					mlog.log(ILogger::logLevel::info, strMsg.str());
					return false;
				}
			}
		}
	}

	auto tl = SessionID::untrusted;
	// Check Admin UIDs
	if (root[strAdminUIDs].isArray())
	{
		const auto& adminUIDs = root[strAdminUIDs];
		if (adminUIDs.size())
		{
			for (Json::ArrayIndex i = 0; i < adminUIDs.size(); i++)
			{
				if (adminUIDs[i].asString().compare(cstrUID) == 0)
					tl = SessionID::trustLevel::uid_authenticated_admin;
			}
		}
	}
	// No Admin, check known player UIDs
	if (tl == SessionID::untrusted)
	{
		if (root[strTrustedUIDs].isArray())
		{
			const auto& trustedUID = root[strTrustedUIDs];
			if (trustedUID.size())
			{
				for (Json::ArrayIndex i = 0; i < trustedUID.size(); i++)
				{
					if (trustedUID[i].asString().compare(cstrUID) == 0)
						tl = SessionID::trustLevel::uid_authenticated;
				}
			}
		}			
	}
	// No Admin, no known player, maybe a guest
	if (tl == SessionID::untrusted)
	{
		if (root[strGuestUIDs].isArray())
		{
			const auto& guestUID = root[strGuestUIDs];
			if (guestUID.size())
			{
				for (Json::ArrayIndex i = 0; i < guestUID.size(); i++)
				{
					if (guestUID[i].asString().compare(cstrUID) == 0)
						tl = SessionID::trustLevel::registered;
				}
			}
		}
	}
	if (tl != SessionID::untrusted)
	{
		SessionID newPlayerSession(cstrUID, g_SessionLiveTime, tl, deflate);
		sessionID = newPlayerSession;		
		g_HTTPSessions.addSession(newPlayerSession);
		// Refresh persistent sessions
		WriteSessions(g_HTTPSessions, getHTTPSessionsFile().c_str()); 
		// Push to SQF
		pushSessions2JsonContainer(g_JSQFSessions, g_HTTPSessions, g_jsonAssocContainer);
		return true;
	}
	else
		return false;
}

string SessionID2JSON(const SessionID& sid)
{
	ModuleLoggerDecorator mlog(__func__, &stdlog);
	Json::Value root;
	root["trustlevel"] = static_cast<int>(sid.getTrustLevel());
	root["ttl"] = sid.getLifeTime();
	root["certified"] = sid.isCertified();  // Certification must be done from SQF side
	root["sessionid"] = sid.ID().c_str();

	string strInfo;
	if (sid.isCertified())
		strInfo = "UID is approved from SQF.";
	else
		strInfo = "Temporary trustlevel. Approval for UID and player identification is pending on SQF.";

	switch (sid.getTrustLevel())
	{
		case SessionID::trustLevel::banned : 
			strInfo.append("WTF - you are banned?!");
			break;
		case SessionID::trustLevel::untrusted :
			strInfo.append("Distrust is the Alpha and Omega of wisdom.");
			break;
		case SessionID::trustLevel::registered :
			strInfo.append("Guests ! You are welcome.");
			break;
		case SessionID::trustLevel::uid_authenticated :
			strInfo.append("Welcome back, borthers in arms.");
			break;
		case SessionID::trustLevel::uid_authenticated_admin :
			strInfo.append("I'm sorry, Dave. I'm afraid i can't do that.");
			break;
		default:
			{
				string strMsg = "Something is going wrong here. Throwing exception...";
				mlog.log(ILogger::logLevel::error, strMsg);
				throw(runtime_error(strMsg));
			}
			break;
	}

	root["info"] = strInfo;	
	Json::FastWriter writer;
	return writer.write(root);
}

static mg_result handle_restful_call_login(struct mg_connection *conn)
{
	ModuleLoggerDecorator mlog(__func__, &stdlog);

	auto session = lookupSessionID(g_HTTPSessions, conn);	
	if (mlog.isLogged(ILogger::logLevel::verbose))
		showSession(mlog, ILogger::logLevel::verbose, session);
	if (session.ID().length())
	{
		g_HTTPSessions.clearSession(session.ID());
		stringstream strInfo;
		strInfo << session.ID() << " logged out, for re-login.";
		mlog.log(ILogger::logLevel::info, strInfo.str());
	}

	char strUID[256];
	const auto getvar_result = mg_get_var(conn, "uid", strUID, sizeof(strUID));
	if (getvar_result < 0)
	{
		SendHTTPError(conn, HttpHeader::sc_BadRequest, "Missing parameter 'uid'", &mlog);
		return MG_FALSE;
	}

	SessionID sid;
	auto resultCreateSession = false;
	try
	{
		resultCreateSession = CreateSessionID(string(strUID), sid, lookupDeflateCompression(conn));
	}
	catch (const exception& e)
	{
		stringstream strMsg;
		strMsg << "Shit happens : " << e.what();
		mlog.log(ILogger::logLevel::error, strMsg.str());
		resultCreateSession = false;
		SendHTTPError(conn, HttpHeader::sc_InternalServerError, strMsg.str(), &mlog);
		return MG_FALSE;
	}

	stringstream strResult;
	if (resultCreateSession)
	{
		stringstream strInfo;
		strInfo << sid.ID() << " logged in.";
		mlog.log(ILogger::logLevel::info, strInfo.str());

		auto strJson = SessionID2JSON(sid);
		std::stringstream strSessionCookie;
		strSessionCookie << "sessionid=" << sid.ID() << "; path =/; expires=" << setGMTDate(sid.getLifeTime());
		string strResult = HttpHeader(HttpHeader::sc_OK,
			vector<HttpHeader::keyVal>({
				HttpHeader::keyVal(HttpHeader::pdhConnectionKeepAlive),
				HttpHeader::keyVal(HttpHeader::pdhDateGMT),
				HttpHeader::keyVal(HttpHeader::pdhSetCookie.key, [&strSessionCookie]() { return strSessionCookie.str(); }),
				HttpHeader::keyVal(HttpHeader::pdhCacheControl.key, []() { return string("no-cache, no-store, must-revalidate"); }),
				HttpHeader::keyVal(HttpHeader::pdhContentType.key, []() { return string("application/json"); }),
				HttpHeader::keyVal(HttpHeader::pdhContentLength.key, [&strJson]() { return t_to_string<size_t>(strJson.length()); })
				})
		).toString();

		mg_write(conn, strResult.c_str(), strResult.length());
		mg_write(conn, strJson.c_str(), strJson.length());
		return MG_TRUE;
	}
	else
	{		
		SendHTTPError(conn, HttpHeader::sc_InternalServerError, "WTF?!", &mlog);
		return MG_FALSE;
	}	
}

static mg_result handle_restful_call_logout(struct mg_connection *conn)
{
	ModuleLoggerDecorator mlog(__func__, &stdlog);
	auto session = lookupSessionID(g_HTTPSessions, conn);
	if (mlog.isLogged(ILogger::logLevel::verbose))
		showSession(mlog, ILogger::logLevel::verbose, session);
	if (session.ID().length())
	{
		g_HTTPSessions.clearSession(session.ID());
		WriteSessions(g_HTTPSessions, getHTTPSessionsFile().c_str());
		pushSessions2JsonContainer(g_JSQFSessions, g_HTTPSessions, g_jsonAssocContainer);
		stringstream strInfo;
		strInfo << session.ID() << " logged out.";
		mlog.log(ILogger::logLevel::info, strInfo.str());
	}

	string strResult = HttpHeader(HttpHeader::sc_OK,
		vector<HttpHeader::keyVal>({
			HttpHeader::keyVal(HttpHeader::pdhDateGMT),
			HttpHeader::keyVal(HttpHeader::pdhContentLength.key, []() { return t_to_string<size_t>(0); })
		})
	).toString();
	
	usleep(1000); // Sleep, to soften DOS attacks
	mg_write(conn, strResult.c_str(), strResult.length());
	return MG_FALSE;
}

static mg_result handle_restful_call_loadjson(struct mg_connection *conn)
{
	ModuleLoggerDecorator mlog(__func__, &stdlog);

	auto session = lookupSessionID(g_HTTPSessions, conn);
	if (mlog.isLogged(ILogger::logLevel::verbose))
		showSession(mlog, ILogger::logLevel::verbose, session);

	if (!session.ID().length())
	{
		SendHTTPError(conn, HttpHeader::sc_Forbidden, "No session ID, not logged in.", &mlog);
		return MG_FALSE;
	}

	char strKey[256];
	auto getvar_result = mg_get_var(conn, "key", strKey, sizeof(strKey));
	if (getvar_result  < 0)
		return MG_FALSE;
	
	if (session.getTrustLevel() < SessionID::trustLevel::uid_authenticated_admin)
	{
		// Compare with list for proctected containers
		if (find(g_httpprotected_keys.begin(), g_httpprotected_keys.end(), string(strKey)) != g_httpprotected_keys.end())
		{
			SendHTTPError(conn, HttpHeader::sc_Forbidden, "'key' is protected for HTTP.", &mlog);
			return MG_FALSE;
		}
	}

	char strFile[256];
	getvar_result = mg_get_var(conn, "file", strFile, sizeof(strFile));
	if (getvar_result < 0)
	{
		SendHTTPError(conn, HttpHeader::sc_BadRequest, "Missing parameter 'file'", &mlog);
		return MG_FALSE;
	}

	stringstream strHeader;
	const string& cstrFile = strFile;
	const auto checkFolderStr = cstrFile.find("..");
	if (checkFolderStr != string::npos)
	{
		SendHTTPError(conn, HttpHeader::sc_Forbidden, "Check your path", &mlog);
		return MG_FALSE;
	}

	auto strJsonfile = getHTTPRootFolder() + g_PathSeparator + cstrFile;
	if (mlog.isLogged(ILogger::logLevel::verbose))
		mlog.log(ILogger::logLevel::verbose, string("Loading Json file :") + strJsonfile);
	
	Json::Value root;
	ifstream contentfile(strJsonfile, ifstream::binary);
	if (!contentfile.good())
	{
		SendHTTPError(conn, HttpHeader::sc_NotFound, " File not found", &mlog);
		return MG_FALSE;
	}
	try
	{
		contentfile >> root;
	}
	catch (const exception& e)
	{
		stringstream strMsg;
		strMsg << "Shit happens : " << e.what();
		mlog.log(ILogger::logLevel::error, strMsg.str());
		SendHTTPError(conn, HttpHeader::sc_InternalServerError, strMsg.str(), &mlog);
		return MG_FALSE;
	}
	catch (...)
	{
		stringstream strMsg;
		strMsg << "Deep shit happens, and no idea how deep.";
		mlog.log(ILogger::logLevel::error, strMsg.str());
		SendHTTPError(conn, HttpHeader::sc_InternalServerError, strMsg.str(), &mlog);
		return MG_FALSE;
	}
	
	Json::FastWriter wbuilder;
	auto strJSON = wbuilder.write(root);
	if (!g_jsonAssocContainer.set(string(strKey), strJSON.c_str()))
		return MG_FALSE;

	stringstream strExpires;
	strExpires << getGMTDate(1).c_str();

	string strResult = HttpHeader(HttpHeader::sc_OK,
		vector<HttpHeader::keyVal>({
				HttpHeader::keyVal(HttpHeader::pdhCacheControl.key, []() { return string("no-cache, no-store, must-revalidate"); }),
				HttpHeader::keyVal(HttpHeader::pdhPragma.key, []() { return string("no-cache"); }),
				HttpHeader::keyVal(HttpHeader::pdhConnectionKeepAlive),
				HttpHeader::keyVal(HttpHeader::pdhDateGMT),
				HttpHeader::keyVal(HttpHeader::pdhExpires.key, [&strExpires]() { return strExpires.str(); }),
				HttpHeader::keyVal(HttpHeader::pdhContentType.key, []() { return string("application/json"); }),
				HttpHeader::keyVal(HttpHeader::pdhContentLength.key, [&strJSON]() { return t_to_string<size_t>(strJSON.length());  })
			})
		).toString();

	mg_write(conn, strResult.c_str(), strResult.length());
	mg_write(conn, strJSON.c_str(), strJSON.length());
	return MG_FALSE;
}


static mg_result restful_api_request(struct mg_connection *conn, const string& func)
{
	auto result = MG_FALSE;
	struct restapi_struct { const char* api_name; mg_result(*handler)(mg_connection *); };
	const  restapi_struct restapi[] = {
		{ "getct", handle_restful_call_getct },
		{ "setct", handle_restful_call_setct },
		{ "mergect", handle_restful_call_mergect },
		{ "loadjson", handle_restful_call_loadjson },
		{ "login", handle_restful_call_login },
		{ "logout", handle_restful_call_logout }
	};

	for (size_t i = 0; i < sizeof(restapi) / sizeof(restapi[0]); i++)
		if (string(restapi[i].api_name).compare(func) == 0)
			return restapi[i].handler(conn);

	return MG_FALSE;
}


static mg_result http_request(struct mg_connection *conn, const char* strURI)
{	
	ModuleLoggerDecorator mlog(__func__, &stdlog);
	auto session = lookupSessionID(g_HTTPSessions, conn);
	auto cpstrHeaderMimeType = mg_get_mime_type(strURI, "text/html");
	string strMimeType;
	string cstrURI = strURI;
	if (cstrURI.length() <= 1)
	{
		if (cstrURI.compare("/") == 0)
		{
			auto strtestcontentfileindexhtml = getHTTPRootFolder() + "/index.html";
			auto strtestcontentfileindexhtm = getHTTPRootFolder() + "/index.htm";
			ifstream contentfilehtml(strtestcontentfileindexhtml, ios::in | ios::binary | ios::ate);
			ifstream contentfilehtm(strtestcontentfileindexhtml, ios::in | ios::binary | ios::ate);
			if (contentfilehtml.is_open())
				cstrURI = "/index.html";
			else
				if (contentfilehtm.is_open())
					cstrURI = "/index.htm";
		}
		strMimeType = "text/html";
	}	
	else
	{
		if (cpstrHeaderMimeType)
			strMimeType = cpstrHeaderMimeType;
	}
		
	const auto checkFolderStr = cstrURI.find("..");
	if ( checkFolderStr != string::npos )
	{	
		SendHTTPError(conn, HttpHeader::sc_Forbidden, "Check your path", &mlog);
		return MG_FALSE;
	}

	auto strcontentfile = getHTTPRootFolder() + cstrURI;
	if (stdlog.isLogged(ILogger::logLevel::verbose))
		stdlog.log( ILogger::logLevel::verbose, string("Reading file :") + strcontentfile);

	ifstream contentfile(strcontentfile, ios::in | ios::binary | ios::ate);
	if (!contentfile.good())
	{
		SendHTTPError(conn, HttpHeader::sc_NotFound, "", &mlog);
		return MG_FALSE;
	}
	else
	{
		if (stdlog.isLogged(static_cast<int>(ILogger::logLevel::verbose)))
		{
			stringstream strVerbose;
			strVerbose << "Reading requested file " << cstrURI;
			stdlog.log(ILogger::logLevel::verbose, strVerbose.str());
		}
	}

	if (!strMimeType.length())
	{		
		SendHTTPError(conn, HttpHeader::sc_NotFound, "Unknown MIME type", &mlog);
		return MG_FALSE;
	}

	char* memblock = nullptr;
	size_t size = 0;
	if (contentfile.is_open())
	{
		const size_t MAX_FILESIZE_FOR_HTTP = (size_t)2 * (size_t)1024 * (size_t)1024 * (size_t)1024;
		size = static_cast<size_t>(contentfile.tellg());
		if (size > MAX_FILESIZE_FOR_HTTP)
		{
			SendHTTPError(conn, HttpHeader::sc_InsufficientStorage, "File is too large", &mlog);
			return MG_FALSE;
		}
		else
		{
			memblock = new char[size];
			contentfile.seekg(0, ios::beg);
			contentfile.read(memblock, size);
			contentfile.close();
		}
	}
	contentfile.close();

	stringstream strExpires;
	const time_t TTLOffset = 12 * 60 * 60;
	strExpires << getGMTDate(TTLOffset);
	string strHeader = HttpHeader(HttpHeader::sc_OK, vector<HttpHeader::keyVal>({
		HttpHeader::keyVal(HttpHeader::pdhConnectionKeepAlive),
		HttpHeader::keyVal(HttpHeader::pdhDateGMT),
		HttpHeader::keyVal(HttpHeader::pdhExpires.key, [&strExpires]() { return strExpires.str(); }),
		HttpHeader::keyVal(HttpHeader::pdhContentType.key, [&strMimeType]() { return strMimeType; }),
		HttpHeader::keyVal(HttpHeader::pdhContentLength.key, [&size]() { return t_to_string<size_t>(size); })
	})).toString();

	mg_write(conn, strHeader.c_str(), strHeader.length());
	mg_write(conn, memblock, size);
	delete[] memblock;
	return MG_TRUE;
}

bool isAPICall(const char* cstrURI, string& strFunc)
{
	const auto strapi_length = g_strapi.length();
	if (strlen(cstrURI) < strapi_length)
		return false;
	auto strURI = string(cstrURI).substr(0, strapi_length);
	transform(strURI.begin(), strURI.end(), strURI.begin(), ::tolower);
	if (strURI.compare(g_strapi) == 0) {
		strFunc = string(cstrURI).substr(strapi_length, string::npos);
		return true;
	}
	else
		return false;
}

static int ev_handler(struct mg_connection *conn, enum mg_event ev) {
	switch (ev) 
	{
		case MG_AUTH:
			return MG_TRUE;
		case MG_REQUEST:
			{
				string strFunc;
				if (stdlog.isLogged(ILogger::logLevel::verbose))
				{
					stringstream strLogMsg;
					strLogMsg << "HTTP Request from " << conn->remote_ip << " : " << conn->uri;
					if (conn->query_string)
					{
						if (string(conn->query_string).length() > 0)
							strLogMsg << "?" << conn->query_string;
					};
					stdlog.log(ILogger::logLevel::verbose, strLogMsg.str());			
				}
				if (isAPICall(conn->uri, strFunc))
					return restful_api_request(conn, strFunc);
				else
					return http_request(conn, conn->uri);
			};
			break;	
	case MG_HTTP_ERROR:
			return true;
			break;
	case MG_CLOSE:
		free(conn->connection_param);
		return MG_TRUE;
	default:
		return MG_FALSE;
	}
}

void LoadArmaRCConfig(ILogger* pLog)
{
	ModuleLoggerDecorator mlog(__func__, pLog);
	static const string strErrorArmaRCConfig = "Unable to set up write proteced Object ArmaRC from ./ArmaRCConfig.json";
	static const string strWarningArmaRCConfig = "'ArmaRC' JSON Container is now unprotected, and can be set up from Web as well as SQF";
	Json::Value root;
	ifstream contentfile(getHTTPConfigFile().c_str(), ifstream::binary);
	if (contentfile.good())
	{		
		try
		{
			contentfile >> root;
		}
		catch (const exception& e)
		{
			mlog.log(ILogger::logLevel::error, "Catch case while reading ArmaRC config");
			mlog.log(ILogger::logLevel::error, e.what());
			mlog.log(ILogger::logLevel::error, strErrorArmaRCConfig);
			mlog.log(ILogger::logLevel::error, getHTTPConfigFile());
			mlog.log(ILogger::logLevel::warning, strWarningArmaRCConfig);
			return;
		}
		catch (...)
		{
			mlog.log(ILogger::logLevel::error, "Catch case while reading ArmaRC config");
			mlog.log(ILogger::logLevel::error, strErrorArmaRCConfig);
			mlog.log(ILogger::logLevel::error, getHTTPConfigFile());
			mlog.log(ILogger::logLevel::warning, strWarningArmaRCConfig);
			return;
		}
	}
	else
	{
		mlog.log(ILogger::logLevel::error, "File not found");
		mlog.log(ILogger::logLevel::error, strErrorArmaRCConfig);
		mlog.log(ILogger::logLevel::error, getHTTPConfigFile());
		mlog.log(ILogger::logLevel::warning, strWarningArmaRCConfig);
		return;
	}

	Json::FastWriter wbuilder;
	auto strJSON = wbuilder.write(root);
	auto succ = g_jsonAssocContainer.set(string("ArmaRC"), strJSON.c_str(), true);
	if (!succ)
	{
		mlog.log(ILogger::logLevel::error, strErrorArmaRCConfig );
		mlog.log(ILogger::logLevel::error, getHTTPConfigFile());
		mlog.log(ILogger::logLevel::warning, strWarningArmaRCConfig);
	}
	else
	{
		if (mlog.isLogged(ILogger::logLevel::info))
		{
			string strMsg = "Reading from " + getHTTPConfigFile();
			mlog.log(ILogger::logLevel::info, strMsg);
		}
	}

	// Lookup for protected keys
	auto& jHTTPProtected = root["httpprotected"];
	if (jHTTPProtected.isArray())
	{
		g_httpprotected_keys.clear();
		for (Json::ArrayIndex i = 0; i < jHTTPProtected.size(); i++)
			g_httpprotected_keys.push_back(jHTTPProtected[i].asString());
	}

	// Lookup for lport
	auto jListenPort = root["lport"];
	if (jListenPort.isInt())
		g_httpPort = jListenPort.asInt();

	// Lookup for Cookie life time
	auto jCookieTTL = root["sessionttl"];
	if (jCookieTTL.isInt())
		g_SessionLiveTime = jCookieTTL.asInt();

	// Lookup for document root
	auto jDocRoot = root["document_root"];
	if (jDocRoot.isString())
		g_HTTPDocumentRoot = jDocRoot.asString();
}

void HTTPServer(){
	ModuleLoggerDecorator log(__func__, &stdlog);
	srand(static_cast<unsigned int>(time(nullptr)));

	if (!createComomAppFolder())
	{
		log.log(ILogger::logLevel::error, "Unable to open or create ArmaHTTP App folder. Giving up!");
		exit(-1);
		return;
	}

	LoadArmaRCConfig(&log);

	if (!ReadSessions(getHTTPSessionsFile().c_str(), g_HTTPSessions))
	{
		auto strWarning = "Unable to open persistent sessions " + getHTTPSessionsFile();
		log.log(ILogger::logLevel::warning, strWarning);
	}
	else
	{		
		if (log.isLogged(ILogger::logLevel::info))
		{
			stringstream strInfo;
			strInfo << "Recovered persistent sessions from " << getHTTPSessionsFile() << " done.";
			strInfo << g_HTTPSessions.size() << " Sessions restored.";
			log.log(ILogger::logLevel::info, strInfo.str());
		}
	}
	
	pushSessions2JsonContainer(g_JSQFSessions, g_HTTPSessions, g_jsonAssocContainer);

	stringstream strSrvPort;
	strSrvPort << g_httpPort;
	g_ServerRunning.exchange(true);
	struct mg_server* http_server;
	log.log(ILogger::logLevel::info, string("Create listening port on " + strSrvPort.str()));
	http_server = mg_create_server(nullptr, ev_handler);
	mg_set_option(http_server, "listening_port", strSrvPort.str().c_str());
	mg_set_option(http_server, "document_root", getHTTPRootFolder().c_str());
	auto listening_port = mg_get_option(http_server, "listening_port");
	
	log.log( ILogger::logLevel::info, "Start.");
	while (!g_exitServer)
	{
		mg_poll_server(http_server, 2000);		
	}	
	// Cleanup, and free server instance
	mg_destroy_server(&http_server);
	g_ServerRunning.exchange(false);
	log.log(ILogger::logLevel::info, "Shutdown");
}

void startArmaHTTPServer()
{	
	ModuleLoggerDecorator log(__func__, &stdlog);
	if (!g_ServerRunning.load())
	{
		log.log(ILogger::logLevel::info, "Start HTTP.");
		g_exitServer = false;
		thread httpServerThread(HTTPServer);		
		httpServerThread.detach();
	}
	else
		log.log(ILogger::logLevel::info, "Start HTTP, but HTTP Server is already running.");
}

void stopArmaHTTPServer()
{
	ModuleLoggerDecorator log(__func__, &stdlog);
	log.log(ILogger::logLevel::info, "Stop HTTP.");
	g_exitServer = true;
	while (g_ServerRunning.load())
		usleep(1000);
}

bool isArmaHTTPServerRunning()
{
	return g_ServerRunning.load();
}
