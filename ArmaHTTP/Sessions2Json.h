/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _H_Session2Json
#define _H_Session2Json
#include <json/json.h>

class SessionID;
class Sessions;

void SessionID2Json(SessionID& src, Json::Value& root);
void SessionID2JSQF(SessionID& src, Json::Value& root);

void Sessions2Json(Sessions& src, Json::Value& root);
void Sessions2JSQF(Sessions& src, Json::Value& root);

void Json2Sessions(Json::Value& root, Sessions& dest);
bool WriteSessions(Sessions& src, const char* cstrFilename);
bool ReadSessions(const char* cstrFilename, Sessions& dest);

#endif
