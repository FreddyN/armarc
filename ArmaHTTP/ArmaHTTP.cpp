/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/


#include "stdafx.h"
#include <string>
#include <sstream>
#include <tlhelp32.h>
#include <tchar.h>
#include <vector>
#include "JsonAssocContainer.h"
#include "armahttpserver.h"
#include "StdLogger.h"
#include "ModuleLoggerDecorator.h"

using std::string;
using std::stringstream;
using std::exception;
using std::map;
using std::vector;


extern JsonAssocContainer g_jsonAssocContainer;
extern map< string, JsonAssocContainer> g_userlocal_JsonAssocContainer;
#define BUFSIZE 32768

// Maximum UIDs
static size_t MAXUIDS = 100;

static const char g_strError[] = "ERROR";
static const char g_strOk[] = "OK";
static const char g_strrConsoleTitle[] = "ArmaHTTP - WebServices for Arma3 Gameplay presented by Banana-Squad";
static string g_processName;
#ifdef _DEBUG
ILogger::logLevel g_logLevel = ILogger::logLevel::verbose;
#else
ILogger::logLevel g_logLevel = ILogger::logLevel::info;
#endif

StdLogger stdlog(g_logLevel);

DWORD WINAPI InstanceThread(LPVOID);
VOID GetAnswerToRequest(const string&, string& );

BOOL GetProcessList(vector< string >& processList)
{
	HANDLE hProcessSnap;
	HANDLE hProcess;
	PROCESSENTRY32 pe32;

	// Take a snapshot of all processes in the system.
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
		return FALSE;

	// Set the size of the structure before using it.
	pe32.dwSize = sizeof(PROCESSENTRY32);

	// Retrieve information about the first process,
	// and exit if unsuccessful
	if (!Process32First(hProcessSnap, &pe32))
	{
		CloseHandle(hProcessSnap);          // clean the snapshot object
		return FALSE;
	}

	do
	{
		hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pe32.th32ProcessID);
		if ( hProcess != NULL )
		{
			processList.push_back(string(pe32.szExeFile));
			CloseHandle(hProcess);
		}
	} while (Process32Next(hProcessSnap, &pe32));

	CloseHandle(hProcessSnap);
	return(TRUE);
}

void preventFork(const string& currentProcessName)
{
	//Isolate process file name;
	const char fileSeparator = '\\';
	string strAuxCurrentProcessName = currentProcessName;
	const size_t lastSeparator = currentProcessName.find_last_of(fileSeparator);
	
	if (lastSeparator != string::npos)
		strAuxCurrentProcessName = currentProcessName.substr(lastSeparator + 1);

	vector< string > processList;
	if (GetProcessList(processList))
	{
		size_t count = 0;
		for (auto process : processList)
		{
			if (process.compare(strAuxCurrentProcessName) == 0)
				count++;
		}
		if (count > 1)
		{
			stringstream  strErr;
			strErr << strAuxCurrentProcessName << " process is already running.";
			stdlog.log(ILogger::logLevel::error, strErr.str());
			exit(-1);
		}
	}
}

bool cmdprocessing(const int argc, const char** argv)
{
	int i = 1;
	while ((i < argc) && (argv[i] != NULL))
	{
		const char* arg = argv[i];
		if (arg)
		{
			const string strArg = arg;
			if (strArg.compare("-l") == 0)
			{
				// Next Argument is the log level
				const char* cstrLogLevel = argv[++i];
				if (cstrLogLevel)
					stdlog.setLogLevel(atoi(cstrLogLevel));
				else
				{
					stdlog.log(ILogger::logLevel::error, "Missing log level argument");
					return false;
				}
			}
		}
		i++;
	}
	return true;
}

int _tmain(const int argc, const char** argv)
{
	if (argc > 1)
	{
		auto succ = cmdprocessing(argc, argv);
		if (!succ)
		{
			stdlog.log(ILogger::logLevel::error, "Error occured. Giving up.");
			return -1;
		}
	}
	preventFork( string( argv[0]) );
	BOOL fConnected(FALSE);
	DWORD dwThreadId = 0;
	HANDLE hPipe(INVALID_HANDLE_VALUE);
	HANDLE hThread(NULL);

	startArmaHTTPServer();
	
	// Waiting for HTTP Server
	auto countdown = 100;
	while (!isArmaHTTPServerRunning() && (countdown > 0)) {
		Sleep(100);
		countdown--;
	}	

	if (!isArmaHTTPServerRunning())
		stdlog.log(ILogger::logLevel::error, "HTTP Server did not start.");
	else
		stdlog.log(ILogger::logLevel::info, "HTTP Server start");	
	SetConsoleTitle(g_strrConsoleTitle);	
	LPTSTR lpszPipename = TEXT("\\\\.\\pipe\\ArmaRC");	
	stdlog.log(ILogger::logLevel::info, string("Listen on:") + string(lpszPipename));
		
	for (;;)
	{		
		hPipe = CreateNamedPipe(
			lpszPipename,             // pipe name 
			PIPE_ACCESS_DUPLEX,       // read/write access 
			PIPE_TYPE_MESSAGE |       // message type pipe 
			PIPE_READMODE_MESSAGE |   // message-read mode 
			PIPE_WAIT,                // blocking mode 
			PIPE_UNLIMITED_INSTANCES, // max. instances  
			BUFSIZE,                  // output buffer size 
			BUFSIZE,                  // input buffer size 
			0,                        // client time-out 
			NULL);                    // default security attribute 

		if (hPipe == INVALID_HANDLE_VALUE)
		{
			stringstream strErr;
			strErr << "CreateNamedPipe failed, GLE=" << GetLastError();
			stdlog.log(ILogger::logLevel::error, strErr.str());
			return -1;
		}

		fConnected = ConnectNamedPipe(hPipe, NULL) ?
		TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);

		if (fConnected)
		{
			hThread = CreateThread(
				NULL,              // no security attribute 
				0,                 // default stack size 
				InstanceThread,    // thread proc
				(LPVOID)hPipe,    // thread parameter 
				0,                 // not suspended 
				&dwThreadId);      // returns thread ID 

			if (hThread == NULL)
			{
				stringstream strErr;
				strErr << "CreateThread failed, GLE=" << GetLastError();
				stdlog.log(ILogger::logLevel::error, strErr.str());
				return -1;
			}
			else CloseHandle(hThread);
		}
		else
			// The client could not connect, so close the pipe. 
			CloseHandle(hPipe);
	}
}

DWORD WINAPI InstanceThread(LPVOID lpvParam)
{
	ModuleLoggerDecorator log("InstanceThread(..):", &stdlog);

	HANDLE hHeap = GetProcessHeap();
	TCHAR* pchRequest = (TCHAR*)HeapAlloc(hHeap, 0, BUFSIZE*sizeof(TCHAR));
	TCHAR* pchReply = (TCHAR*)HeapAlloc(hHeap, 0, BUFSIZE*sizeof(TCHAR));

	DWORD cbBytesRead(0);	
	DWORD cbWritten(0);
	BOOL fSuccess(FALSE);
	HANDLE hPipe(NULL);


	if (lpvParam == nullptr)
	{
		stringstream strErr;
		strErr << "Pipe Server Failure: InstanceThread got an unexpected NULL value in lpvParam. InstanceThread exitting.";
		log.log(ILogger::logLevel::error, strErr.str());
		if (pchReply != nullptr) HeapFree(hHeap, 0, pchReply);
		if (pchRequest != nullptr) HeapFree(hHeap, 0, pchRequest);
		return DWORD(-1);
	}

	if (pchRequest == nullptr)
	{
		stringstream strErr;
		strErr << "Pipe Server Failure: InstanceThread got an unexpected NULL heap allocation. InstanceThread exitting.";
		log.log(ILogger::logLevel::error, strErr.str());
		if (pchReply != nullptr) HeapFree(hHeap, 0, pchReply);
		return DWORD(-1);
	}

	if (pchReply == nullptr)
	{
		stringstream strErr;
		strErr << "Pipe Server Failure: InstanceThread got an unexpected NULL heap allocation. InstanceThread exitting.";
		log.log(ILogger::logLevel::error, strErr.str());
		if (pchRequest != nullptr) HeapFree(hHeap, 0, pchRequest);
		return DWORD(-1);
	}
	
	hPipe = HANDLE(lpvParam);

	while (1)
	{
		ZeroMemory(pchRequest, BUFSIZE*sizeof(TCHAR));
		fSuccess = ReadFile(
			hPipe,        // handle to pipe 
			pchRequest,    // buffer to receive data 
			BUFSIZE*sizeof(TCHAR), // size of buffer 
			&cbBytesRead, // number of bytes read 
			NULL);        // not overlapped I/O 

		if (!fSuccess || cbBytesRead == 0)
		{
			if (GetLastError() != ERROR_BROKEN_PIPE)
			{
				stringstream strErr;
				strErr << "InstanceThread ReadFile failed, GLE=" << GetLastError();
				log.log(ILogger::logLevel::error, strErr.str());
			} 
			else
			{
				stringstream strErr;
				strErr << "Client disconnect.";
				log.log(ILogger::logLevel::verbose, strErr.str());
			}
			break;
		}

		// Process the incoming message.
		const string cstrRequest(pchRequest, cbBytesRead);
		string strReply;
		GetAnswerToRequest(cstrRequest, strReply);

		// Write the reply to the pipe. 
		fSuccess = WriteFile(
			hPipe,        // handle to pipe 
			strReply.c_str(),     // buffer to write from 
			strReply.length(), // number of bytes to write 
			&cbWritten,   // number of bytes written 
			NULL);        // not overlapped I/O 

		if (!fSuccess || strReply.length() != cbWritten)
		{
			stringstream strErr;
			strErr << "InstanceThread WriteFile failed, GLE=" << GetLastError();
			log.log(ILogger::logLevel::error, strErr.str());
			break;
		}
	}

	FlushFileBuffers(hPipe);
	DisconnectNamedPipe(hPipe);
	CloseHandle(hPipe);

	HeapFree(hHeap, 0, pchRequest);
	HeapFree(hHeap, 0, pchReply);
	return 1;
}

VOID GetAnswerToRequest(const string& strReq, string& strReply)
{		
	
	ModuleLoggerDecorator log("GetAnswerToRequest", &stdlog);
	const auto separator_function = strReq.find_first_of(" ");
	const auto& cstrFunction = strReq.substr(0, separator_function);

	if (separator_function == string::npos)
	{
		log.log(ILogger::logLevel::error, string("No function separator"));
		strReply = g_strError;
		return;
	}

	if (cstrFunction.compare("WriteJsonCache") == 0)
	{
		ModuleLoggerDecorator mlog("WriteJsonCache", &log);
		const auto& cstrParams = strReq.substr(separator_function + 1);
		const auto separator_params = cstrParams.find_first_of(" ");

		if (separator_params == string::npos)
		{
			mlog.log(ILogger::logLevel::error, string("No parameter separator"));
			strReply = g_strError;
			return;
		}

		const auto& cstrName = cstrParams.substr(0, separator_params);
		const auto& cstrJson = cstrParams.substr(separator_params + 1);

		if (!g_jsonAssocContainer.set(cstrName, cstrJson))
		{
			mlog.log(ILogger::logLevel::error, "JSON Parsing rejected");
			mlog.log(ILogger::logLevel::warning, string("INVALID_CONTENT:") + cstrJson);
			strReply = g_strError;
		}
		else
		{
			if (mlog.getLogLevel() > ILogger::logLevel::info)
			{

				mlog.log(ILogger::logLevel::verbose, string("Container/Object:") + cstrName);
				mlog.log(ILogger::logLevel::verbose, string("JSON_CONTENT:") + cstrJson);
			}
			strReply = g_strOk;
		}
		
	}
	else
	if (cstrFunction.compare("MergeJsonCache") == 0)
	{
		ModuleLoggerDecorator mlog("MergeJsonCache", &log);
		const auto& cstrParams = strReq.substr(separator_function + 1);
		const auto separator_params = cstrParams.find_first_of(" ");

		if (separator_params == string::npos)
		{
			mlog.log(ILogger::logLevel::error, string("No parameter separator"));
			strReply = g_strError;
			return;
		}

		const auto& cstrName = cstrParams.substr(0, separator_params);
		const auto& cstrJsonToMerge = cstrParams.substr(separator_params + 1);

		if (g_jsonAssocContainer.merge(cstrName, cstrJsonToMerge))
		{
			if (mlog.getLogLevel() > ILogger::logLevel::info)
			{
				mlog.log(ILogger::logLevel::verbose, string("Container/Object:") + cstrName);
				mlog.log(ILogger::logLevel::verbose, string("JSON_CONTENT:") + cstrJsonToMerge);
			}
			strReply = g_strOk;
		}
		else
		{			
			mlog.log(ILogger::logLevel::error, "JSON Parsing rejected.");
			mlog.log(ILogger::logLevel::warning, string("INVALID_CONTENT:") + cstrJsonToMerge);
			strReply = g_strError;
		}
	}
	else
	if (cstrFunction.compare("GetValue") == 0)
	{
		ModuleLoggerDecorator mlog("GetchValue", &log);
		const auto& cstrParams = strReq.substr(separator_function + 1);
		const auto separator_params = cstrParams.find_first_of(" ");

		if (separator_params == string::npos)
		{
			mlog.log(ILogger::logLevel::error, string("No parameter separator"));
			strReply = g_strError;
			return;
		}

		const auto& cstrName = cstrParams.substr(0, separator_params);
		const auto& cstrPath = cstrParams.substr(separator_params + 1);

		string results;
		try {

			if (g_jsonAssocContainer.getValue(cstrName, cstrPath, results))
			{
				strReply = results;
			}
			else
				strReply = g_strError;
		}
		catch (const exception& e)
		{
			std::stringstream strErr;
			strErr << "Catch case " << e.what() << " for " << cstrName << "(" << cstrPath << ")";
			log.log(ILogger::logLevel::error, strErr.str());
			strReply = g_strError;
		}
	}
	else
	if (cstrFunction.compare("FetchValue") == 0)
	{
		ModuleLoggerDecorator mlog("FetchValue", &log);
		const auto& cstrParams = strReq.substr(separator_function + 1);
		const auto separator_params = cstrParams.find_first_of(" ");

		if (separator_params == std::string::npos)
		{
			mlog.log(ILogger::logLevel::error, string("No parameter separator"));
			strReply = g_strError;
			return;
		}

		const auto& cstrName = cstrParams.substr(0, separator_params);
		const auto& cstrPath = cstrParams.substr(separator_params + 1);
		string results;

		try
		{
			if (g_jsonAssocContainer.fetchValue(cstrName, cstrPath, results))
			{
				strReply = results;
				//StringVector2SQF(results, strReply);
			}
			else
				strReply = g_strError;
		}
		catch (const exception& e)
		{
			stringstream strErr;
			strErr << "Catch case " << e.what() << " for " << cstrName << "(" << cstrPath << ")";
			mlog.log(ILogger::logLevel::error, strErr.str());
			strReply = g_strError;
		}
	}
	else
		strReply = g_strError;
}
