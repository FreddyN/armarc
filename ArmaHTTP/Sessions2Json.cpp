/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <string>
#include <fstream>
#include "Sessions2Json.h"
#include "SessionID.h"
#include "Sessions.h"

using std::string;
using std::time_t;
using std::map;
using std::ofstream;
using std::ifstream;
using std::ios;

void SessionID2Json(SessionID& src, Json::Value& root)
{
	root["sid"] = src.ID();
	root["uid"] = src.UID();
	root["lifetime"] = static_cast<Json::Int64>(src.getLifeTime());
	root["trustlevel"] = static_cast<int>(src.getTrustLevel());
	root["certified"] = src.isCertified();
	root["deflate"] = src.isSupportingDeflate();
}

SessionID Json2SessionID(const Json::Value& root)
{
	string strSID = "";
	string strUID = "";
	time_t lifetime = 0;
	auto trustlevel = SessionID::trustLevel::untrusted;
	auto certified = false;
	auto deflate = false;

	auto jSID = root["sid"];
	auto jUID = root["uid"];
	auto jLifetime = root["lifetime"];
	auto jTrustlevel = root["trustlevel"];
	auto jCertified = root["certified"];
	auto jDeflate = root["deflate"];
	if (jSID.isString())
		strSID = jSID.asString();

	if (jUID.isString())
		strUID = jUID.asString();

	if (jLifetime.isNumeric())
		lifetime = jLifetime.asInt64();

	if (jTrustlevel.isNumeric())
		trustlevel = static_cast<SessionID::trustLevel>(jTrustlevel.asInt());

	if (jCertified.isBool())
		certified = jCertified.asBool();
	
	if (jDeflate.isBool())
		deflate = jDeflate.asBool();

	return SessionID(strSID, strUID, lifetime, trustlevel, certified, deflate);
}

void Sessions2Json(Sessions& src, Json::Value& root)
{
	auto src2(src);	
	const auto& sessions = src2.getMap();
	for (auto session : sessions)
	{
		Json::Value jSession;
		SessionID2Json(session.second, jSession);
		root.append(jSession);
	}
}

void Json2Sessions(Json::Value& root, Sessions& dest)
{
	if (!root.isArray())
		return;	
	for (Json::ArrayIndex i = 0; i < root.size(); i++)
		dest.addSession(Json2SessionID(root[i]));	
}


bool WriteSessions(Sessions& src, const char* cstrFilename)
{
	ofstream jFile(cstrFilename, ofstream::binary);
	if (!jFile.good())
		return false;

	Json::Value root;
	Sessions2Json(src, root);
	Json::StyledWriter writer;

	auto jstr = writer.write(root);
	jFile.write(jstr.c_str(), jstr.length());
	jFile.close();
	return true;
}

bool ReadSessions(const char* cstrFilename, Sessions& dest)
{
	ifstream jFile(cstrFilename, ios::in | ios::binary | ios::ate);
	if (!jFile.is_open())
		return false;

	
	auto size = static_cast<size_t>(jFile.tellg());
	auto memblock = new char[size];
	jFile.seekg(0, ios::beg);
	jFile.read(memblock, size);
	jFile.close();

	string strJson(memblock, size);
	delete [] memblock;

	Json::Reader jsr;
	Json::Value	 root;
	if (!jsr.parse(strJson, root))
		return false;

	Json2Sessions(root, dest);
	return true;
}

void SessionID2JSQF(SessionID& src, Json::Value& root)
{
	Json::Value jSID = src.ID();
	Json::Value jUID = src.UID();
	Json::Value jTrustlevel = static_cast<int>(src.getTrustLevel());
	Json::Value jCertified = static_cast<int>(src.isCertified() ? 1 : 0);	
	root.append(jSID);
	root.append(jUID);
	root.append(jTrustlevel);
	root.append(jCertified);
	
}

void Sessions2JSQF(Sessions& src, Json::Value& root)
{
	auto src2(src);
	const auto& sessions = src2.getMap();
	Json::Value jSessions;
	for (auto session : sessions)
	{
		Json::Value jSession;
		SessionID2JSQF(session.second, jSession);
		jSessions.append(jSession);
	}
	root["sessions"] = jSessions;
}
