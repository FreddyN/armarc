/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_SessionID
#define _H_SessionID
#include <string>
#include <ctime>

using std::string;
using std::time_t;

/**********************************************************************************************//**
 * \class	SessionID
 *
 * \brief	A session identifier.
 *
 * \author	Friedrich
 * \date	05.09.2015
 **************************************************************************************************/

class SessionID
{
public:

	/**********************************************************************************************//**
	 * \enum	trustLevel
	 *
	 * \brief	Values that represent trust levels.
	 **************************************************************************************************/

	enum trustLevel { banned = -2, untrusted = -1, registered = 0, uid_authenticated = 1, uid_authenticated_admin = 2 };

	/**********************************************************************************************//**
	 * \fn	SessionID::SessionID();
	 *
	 * \brief	Default constructor.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 **************************************************************************************************/

	SessionID();

	/**********************************************************************************************//**
	 * \fn	SessionID::SessionID(const string& cstrSID, const string& cstrUID, time_t timetolive, trustLevel tl, bool certified, bool deflate);
	 *
	 * \brief	Constructor.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	cstrSID   	The cstr SID.
	 * \param	cstrUID   	The cstr UID.
	 * \param	timetolive	The timetolive.
	 * \param	tl		  	The tl.
	 * \param	certified 	true if certified.
	 * \param	deflate   	true to deflate.
	 **************************************************************************************************/

	SessionID(const string& cstrSID, const string& cstrUID, time_t timetolive, trustLevel tl, bool certified, bool deflate);

	/**********************************************************************************************//**
	 * \fn	SessionID::SessionID(const string& cstrUID, int timetolive = 60, trustLevel trustlevel = registered, bool deflate = false);
	 *
	 * \brief	Constructor.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	cstrUID   	The cstr UID.
	 * \param	timetolive	The timetolive.
	 * \param	trustlevel 	The trust level.
	 * \param	deflate   	true to deflate.
	 **************************************************************************************************/

	SessionID(const string& cstrUID, int timetolive = 60, trustLevel trustlevel = registered, bool deflate = false);

	/**********************************************************************************************//**
	 * \fn	SessionID::SessionID(const SessionID& src);
	 *
	 * \brief	Copy constructor.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	src	Source to copy from
	 **************************************************************************************************/

	SessionID(const SessionID& src);

	/**********************************************************************************************//**
	 * \fn	SessionID::~SessionID();
	 *
	 * \brief	Destructor.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 **************************************************************************************************/

	~SessionID();

	/**********************************************************************************************//**
	 * \fn	const string& SessionID::UID() const;
	 *
	 * \brief	Gets the UID.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \return	A string&amp;
	 **************************************************************************************************/

	const string& UID() const;

	/**********************************************************************************************//**
	 * \fn	const string& SessionID::ID() const;
	 *
	 * \brief	Gets the identifier.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \return	A string&amp;
	 **************************************************************************************************/

	const string& ID() const;

	/**********************************************************************************************//**
	 * \fn	const time_t& SessionID::getLifeTime() const;
	 *
	 * \brief	Gets life time.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \return	The life time.
	 **************************************************************************************************/

	const time_t& getLifeTime() const;

	/**********************************************************************************************//**
	 * \fn	void SessionID::setTrustLevel(trustLevel val);
	 *
	 * \brief	Sets trust level.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	val	The value.
	 **************************************************************************************************/

	void setTrustLevel(trustLevel val);

	/**********************************************************************************************//**
	 * \fn	void SessionID::setCertified(bool val);
	 *
	 * \brief	Setup as certified/non-certified
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	val	true to value.
	 **************************************************************************************************/

	void setCertified(bool val);

	/**********************************************************************************************//**
	 * \fn	bool SessionID::isCertified() const;
	 *
	 * \brief	Query if this object is certified.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \return	true if certified, false if not.
	 **************************************************************************************************/

	bool isCertified() const;

	/**********************************************************************************************//**
	 * \fn	bool SessionID::isSupportingDeflate() const;
	 *
	 * \brief	Query if this object is supporting deflate compression in communication
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \return	true if supporting deflate, false if not.
	 **************************************************************************************************/

	bool isSupportingDeflate() const;

	/**********************************************************************************************//**
	 * \fn	void SessionID::setDeflate(bool deflate);
	 *
	 * \brief	Set deflate compression flag
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	deflate	true to deflate.
	 **************************************************************************************************/

	void setDeflate(bool deflate);

	/**********************************************************************************************//**
	 * \fn	trustLevel SessionID::getTrustLevel() const;
	 *
	 * \brief	Gets trust level.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \return	The trust level.
	 **************************************************************************************************/

	trustLevel getTrustLevel() const;
protected:

	/**********************************************************************************************//**
	 * \fn	void SessionID::_createASessionID();
	 *
	 * \brief	Creates a session identifier.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 **************************************************************************************************/

	void _createASessionID();

protected:
	/** \brief	Identifier for the session. */
	string		_sessionID;
	/** \brief	The UID. */
	string		_UID;
	/** \brief	The live time limitation. */
	time_t		_timetolive;
	/** \brief	The trustlevel. */
	trustLevel	_trustlevel;
	/** \brief	true if certified. */
	bool		_certified;
	/** \brief	true to support deflate. */
	bool		_supportDeflate;
};

#endif
