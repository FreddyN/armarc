/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
// Poor man abstraction for sleep
#ifdef _WIN32
	#include <Windows.h>
	inline void usleep(size_t ms) { Sleep(ms); };
#endif
#include <ctime>
#include "Sessions.h"
#include "Sessions2Json.h"

using std::time_t;
using std::time;
using std::stringstream;

TEST(SessionsID, TimeToLive1)
{
	SessionID sid("WTF", 0);
	usleep(1000); // Wait 2 seconds
	time_t stoptimeTime = time(nullptr);
	time_t creationTime = sid.getLifeTime();
	EXPECT_EQ( creationTime < stoptimeTime, true);		
}

TEST(SessionsID, TimeToLive2)
{
	SessionID sid("WTF", 2);
	usleep(1000); // Wait 2 seconds
	time_t stoptimeTime = time(nullptr);
	time_t creationTime = sid.getLifeTime();
	EXPECT_EQ(creationTime > stoptimeTime, true);
}

TEST(SessionsID, CtorDefaultTrustLevel)
{
	SessionID s;
	EXPECT_LT(s.getTrustLevel(), 0);
}

TEST(Sessions, createSessionID)
{
	Sessions s;
	SessionID sid("WTF");

	ASSERT_EQ(sid.ID().length() > 8, true);

	ASSERT_EQ(s.addSession(sid), true);
	SessionID sid_result;

	EXPECT_EQ(s.getSession(sid.ID(), sid_result), true);
	EXPECT_EQ(sid_result.UID().compare("WTF"), 0);
}

TEST(Sessions, cleanGarbage)
{
	Sessions s;	
	ASSERT_EQ(s.addSession(SessionID("WTF1", 1)), true);
	usleep(2000); // Wait 2 seconds
	ASSERT_EQ(s.addSession(SessionID("WTF2", 4)), true);
	EXPECT_EQ(s.size(), 1); // TTL WTF1 is cleaned
}

TEST(Sessions, CopyCtor)
{
	Sessions s1;
	ASSERT_EQ(s1.addSession(SessionID("WTF1")), true);
	ASSERT_EQ(s1.addSession(SessionID("WTF2")), true);
	Sessions s2(s1);
	EXPECT_EQ(s2.size(), s1.size());
}

TEST(Sessions, getMap)
{
	Sessions s1;
	ASSERT_EQ(s1.addSession(SessionID("WTF1")), true);
	ASSERT_EQ(s1.addSession(SessionID("WTF2")), true);
	Sessions s2(s1);
	EXPECT_EQ(s2.getMap().size(), s1.size());
	EXPECT_EQ(Sessions(s2).getMap().size(), s1.size());
}

TEST(Sessions, clearSession)
{
	Sessions s;
	SessionID	s1("WTF1");
	SessionID	s2("WTF2");
	s.addSession(s1);
	s.addSession(s2);
	s.clearSession(s1.ID());
	EXPECT_EQ(s.size(), 1);
}

TEST(Sessions, updateSessionWithUniqueUID)
{
	Sessions s;
	SessionID	s1("WTF1");
	SessionID	s2("WTF1");	
	s.addSession(s1);
	s.addSession(s2);
	EXPECT_EQ(s.size(), 2);
}

TEST(Sessions, sessionLimitations)
{
	const size_t testLimitation = 10000;
	Sessions s;
	size_t i = 0;
	while (i < testLimitation)
	{
		stringstream strUID;
		strUID << "UID" << i;
		if (!s.addSession(SessionID(strUID.str(), 1440)))
			break;
	}
	EXPECT_LT(s.size(), testLimitation);
	RecordProperty("limitedSessions", s.size());
}

TEST(Sessions, sessionIDLength)
{
	const size_t testLimitation = 100;
	size_t i = 0;
	size_t minlen = 1000000;
	while (i < testLimitation)
	{
		stringstream strUID;
		strUID << "UID" << i++;
		SessionID s(strUID.str(), 1440);
		if (s.ID().length() < minlen)
			minlen = s.ID().length();
	}
	EXPECT_GT(minlen, (size_t) 11);
}

TEST(Session, sessionID2Json)
{
	Json::Value root;
	SessionID s("MySID", "MyUID", 0, SessionID::trustLevel::registered, false, false);
	SessionID2Json(s, root);
	EXPECT_EQ(root["sid"].asString().compare("MySID"), 0);	

#ifdef _DEBUG
	Json::FastWriter writer;
	std::cout << writer.write(root) << std::endl;
#endif
}

TEST(Session, sessions2Json)
{
	Json::Value root;
	time_t lifetime = time(nullptr) + 72000;
	Sessions  sc;
	SessionID s1("MySID1", "MyUID1", lifetime, SessionID::trustLevel::registered, false, false);
	SessionID s2("MySID2", "MyUID2", lifetime, SessionID::trustLevel::registered, false, false);
	SessionID s3("MySID3", "MyUID3", lifetime, SessionID::trustLevel::registered, false, false);
	ASSERT_EQ(sc.addSession(s1), true);
	ASSERT_EQ(sc.addSession(s2), true);
	ASSERT_EQ(sc.addSession(s3), true);
	EXPECT_EQ(sc.size(), 3);
	Sessions2Json(sc, root);
	EXPECT_EQ(root.isArray(), true);
#ifdef _DEBUG
	Json::FastWriter writer;
	std::cout << writer.write(root) << std::endl;
#endif
}

TEST(Session, Persistent_sessions2Json)
{
	static const char testJsonFile[] = "TestSessions.json";
	Json::Value root;
	time_t lifetime = time(nullptr) + 72000; // To "survive" TTL during test, take a long TTL
	Sessions  sc;
	SessionID s1("MySID1", "MyUID1", lifetime, SessionID::trustLevel::registered, false, false);
	SessionID s2("MySID2", "MyUID2", lifetime, SessionID::trustLevel::registered, false, false);
	SessionID s3("MySID3", "MyUID3", lifetime, SessionID::trustLevel::registered, false, false);
	ASSERT_EQ(sc.addSession(s1), true);
	ASSERT_EQ(sc.addSession(s2), true);
	ASSERT_EQ(sc.addSession(s3), true);
	EXPECT_EQ(sc.size(), 3);

	EXPECT_EQ(WriteSessions(sc, testJsonFile), true);

	Sessions sc2;
	EXPECT_EQ(ReadSessions(testJsonFile, sc2), true);
	EXPECT_EQ(sc2.size(), sc.size());
}

TEST(Session, JSQF_SessionID)
{
	Json::Value root;
	SessionID s("MySID", "MyUID", 10000, SessionID::trustLevel::registered, false, false);
	SessionID2JSQF(s, root);
	EXPECT_EQ(root.isArray(), true);
	EXPECT_EQ(root[0].isString(), true);	// SID
	EXPECT_EQ(root[1].isString(), true);	// UID
	EXPECT_EQ(root[2].isInt(), true);		// trustLevel
	EXPECT_EQ(root[3].isInt(), true);		// certified
	
	EXPECT_EQ(root[0].asString().compare("MySID") == 0, true);
	EXPECT_EQ(root[1].asString().compare("MyUID") == 0, true);
	EXPECT_EQ(root[2].asInt() == static_cast<int>(SessionID::trustLevel::registered), true);
	EXPECT_EQ(root[3].asInt() == 0, true);
#ifdef _DEBUG
	Json::FastWriter writer;
	std::cout << writer.write(root) << std::endl;
#endif
}

TEST(Session, JSQF_Session)
{

	time_t lifetime = time(nullptr) + 72000; // To "survive" TTL during test, take a long TTL
	Sessions  sc;
	SessionID s1("MySID1", "MyUID1", lifetime, SessionID::trustLevel::registered, false, false);
	SessionID s2("MySID2", "MyUID2", lifetime, SessionID::trustLevel::uid_authenticated, false, false);
	SessionID s3("MySID3", "MyUID3", lifetime, SessionID::trustLevel::registered, false, false);
	ASSERT_EQ(sc.addSession(s1), true);
	ASSERT_EQ(sc.addSession(s2), true);
	ASSERT_EQ(sc.addSession(s3), true);
	EXPECT_EQ(sc.size(), 3);

	Json::Value root;
	Sessions2JSQF(sc, root);
	EXPECT_EQ(root.isObject(), true);
	EXPECT_EQ(root["sessions"].isArray(), true);
	EXPECT_EQ(root["sessions"].size() == 3, true);
	const Json::Value& jSessions = root["sessions"];
	EXPECT_EQ(jSessions[0][0].asString().compare("MySID1") == 0, true);
	EXPECT_EQ(jSessions[1][0].asString().compare("MySID2") == 0, true);
	EXPECT_EQ(jSessions[2][0].asString().compare("MySID3") == 0, true);
	EXPECT_EQ(jSessions[1][2].asInt(), static_cast<int>(SessionID::trustLevel::uid_authenticated));
#ifdef _DEBUG
	Json::FastWriter writer;
	std::cout << writer.write(root) << std::endl;
#endif
}
