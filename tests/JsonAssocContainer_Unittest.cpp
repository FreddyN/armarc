/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <sstream>
#include <json/json.h>
#include "JsonAssocContainer.h"

using std::string;
using std::stringstream;

static string	testJson = "{ \"Object1\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" } }";

TEST(JsonAssocContainer, fetchValue_Case1)
{
	Json::Reader	jsr;
	Json::Value		root;

	ASSERT_EQ(jsr.parse(testJson, root, false), true);

	JsonAssocContainer	myContainer;
	myContainer.set("myKey", testJson);

	string values;
	bool result = myContainer.fetchValue("myKey", "Object1/Key1", values);
	ASSERT_EQ(result, true);	

	// Test content
	string strCheckJson;
	Json::Value	root2;
	myContainer.getCopy("myKey", strCheckJson);
	ASSERT_EQ(jsr.parse(strCheckJson, root2, false), true);
	EXPECT_EQ(root2["Object1"]["Key2"].asString().compare("Value2"), 0);
	EXPECT_EQ(root2["Object1"]["Key1"], Json::Value::null);
};


TEST(JsonAssocContainer, getValue_Case1)
{
	Json::Reader	jsr;
	Json::Value		root;

	ASSERT_EQ(jsr.parse(testJson, root, false), true);

	JsonAssocContainer	myContainer;
	myContainer.set("myKey", testJson);

	string values;
	bool result = myContainer.fetchValue("myKey", "Object1/Key1", values);
	ASSERT_EQ(result, true);
}

TEST(JsonAssocContainer, merge_Case1)
{
	static string	testJson1 = "{ \"Object1\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" } }";
	static string	testJson2 = "{ \"Object1\" : { \"Key3\" : \"Value3\", \"Key4\" : \"Value4\" } }";
	JsonAssocContainer	myContainer;
	myContainer.set("myObject", testJson1);
	ASSERT_EQ( myContainer.merge("myObject", testJson2), true );

	Json::Reader	jsr;
	Json::Value		root;
	string		testJson3;
	ASSERT_EQ(myContainer.getCopy("myObject", testJson3), true);
	ASSERT_EQ(jsr.parse(testJson3, root), true);
	EXPECT_EQ(root["Object1"]["Key4"].asString().compare("Value4"), 0);
}

TEST(JsonAssocContainer, merge_Case2)
{
	static string	testJson1 = "{ \"Object1\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" , \"Object2\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" } } }";
	static string	testJson2 = "{ \"Object1\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" , \"Object2\" : { \"Key3\" : \"Value3\", \"Key4\" : \"Value4\" } } }";
	Json::Reader	jsr;
	Json::Value		testroot;

	ASSERT_EQ(jsr.parse(testJson1, testroot), true);
	ASSERT_EQ(jsr.parse(testJson2, testroot), true);

	JsonAssocContainer	myContainer;
	myContainer.set("myObject", testJson1);
	ASSERT_EQ(myContainer.merge("myObject", testJson2), true);

	Json::Value		root;
	string		testJson3;
	ASSERT_EQ(myContainer.getCopy("myObject", testJson3), true);
	ASSERT_EQ(jsr.parse(testJson3, root), true);
	EXPECT_EQ(root["Object1"]["Object2"]["Key4"].asString().compare("Value4"), 0);
}

TEST(JsonAssocContainer, merge_PreventDOSAttack)
{
	const size_t objectsToTest = 100000;
	static string	testJson1 = "{ \"Object1\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" , \"Object2\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" } } }";
	JsonAssocContainer	myContainer;

	size_t successCounter = 0;
	for (size_t i = 0; i < objectsToTest; i++)
	{
		stringstream strNameID;
		strNameID << i;
		if (myContainer.set(strNameID.str(), testJson1))
			successCounter++;
		else
			break;
	}
	EXPECT_EQ(successCounter  < objectsToTest, true);
}


TEST(JsonAssocContainer, ContainerWriteProtection)
{
	const static string	testJson1 = "{ \"Object1\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" , \"Object2\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" } } }";
	JsonAssocContainer	myContainer;
	ASSERT_EQ(myContainer.set("WriteProtectionTest", testJson1, true), true);
	EXPECT_EQ(myContainer.set("WriteProtectionTest", testJson1), false);	
	EXPECT_EQ(myContainer.set("WriteProtectionTest", testJson1, true), false);
}


TEST(JsonAssocContainer, JsonReaderWriterForStrings)
{
	Json::Value root;
	root["key"] = string("\"Val\"ue/\\");
	Json::FastWriter writer;
	const string result = writer.write(root);	
	Json::Value root2;
	Json::Reader jsonReader;
	jsonReader.parse(result, root2);
	EXPECT_EQ(root["key"].asString().compare( root2["key"].asString()), 0);
}
