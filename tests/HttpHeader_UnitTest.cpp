/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include <ctime>
#include <iomanip>
#include "HttpHeader.h"
#include "t_to_string.h"

using std::put_time;
using std::time;
using std::time_t;
using std::stringstream;

TEST(HttpHeader, HeaderSeparator)
{

	string testJsonContent = "{ \"Key\": \"Value\" }";
	string result = HttpHeader(HttpHeader::sc_OK,
		vector<HttpHeader::keyVal>({
			HttpHeader::keyVal(HttpHeader::pdhCacheControl.key, []() { return string("no-cache, no-store, must-revalidate"); }),
			HttpHeader::keyVal(HttpHeader::pdhPragma.key, []() { return string("no-cache"); }),
			HttpHeader::keyVal(HttpHeader::pdhContentType.key, []() { return string("application/json"); }),
			HttpHeader::keyVal(HttpHeader::pdhContentLength.key, [&testJsonContent]() { return t_to_string < size_t >(testJsonContent.length()); })
	})).toString();
	std::cout << result;


	ASSERT_EQ(result.length() > 2, true);

	// Test for tailing \r\n
	EXPECT_EQ(result[result.length() - 1] == '\n', true);
	EXPECT_EQ(result[result.length() - 2] == '\r', true);	
}

TEST(HttpHeader, RequestKey)
{
	string testJsonContent = "{ \"Key\": \"Value\" }";
	HttpHeader header(HttpHeader::sc_OK,
		vector<HttpHeader::keyVal>({
		HttpHeader::keyVal(HttpHeader::pdhCacheControl.key, []() { return string("no-cache, no-store, must-revalidate"); }),
		HttpHeader::keyVal(HttpHeader::pdhPragma.key, []() { return string("no-cache"); }),
			HttpHeader::keyVal(HttpHeader::pdhContentType.key, []() { return string("application/json"); }),
			HttpHeader::keyVal(HttpHeader::pdhContentLength.key, [&testJsonContent]() { return t_to_string < size_t >(testJsonContent.length()); })
	}));

	std::string val;
	ASSERT_EQ(header.request(HttpHeader::pdhPragma.key, val), true);
	EXPECT_EQ(val.compare("no-cache") == 0, true);

	ASSERT_EQ(header.request(HttpHeader::pdhContentType.key, val), true);
	EXPECT_EQ(val.compare("application/json") == 0, true);

}