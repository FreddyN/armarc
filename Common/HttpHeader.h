/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_HttpHeader
#define _H_HttpHeader

#include <string>
#include <vector>
#include <functional>
using std::string;
using std::vector;

class HttpHeader
{
public:
	
	struct keyVal {
		keyVal( const string& k, std::function<string()> f ):
			key(k),
			func(f)
		{
		};
		string key;
		std::function<string()> func;
	};

	class statuscode {
	public:
		statuscode(int s, const string& hint);
		statuscode(const statuscode&);
		int val;
		string text;
	};

	HttpHeader();
	explicit HttpHeader(const statuscode& s);
	explicit HttpHeader(const statuscode&, const vector<HttpHeader::keyVal>&);
	~HttpHeader();
	void add(const keyVal& item);
	bool request(const string& key, string& val) const;
	string toString() const;

	const static keyVal pdhConnection;
	const static keyVal pdhConnectionKeepAlive;
	const static keyVal pdhDateGMT;
	const static keyVal pdhPragma;
	const static keyVal pdhContentType;
	const static keyVal pdhContentLength;
	const static keyVal pdhContentEncodingDeflate;
	const static keyVal pdhCacheControl;
	const static keyVal pdhSetCookie;
	const static keyVal pdhExpires;

	const static statuscode sc_OK;
	const static statuscode sc_NotFound;
	const static statuscode sc_Forbidden;
	const static statuscode sc_InternalServerError;
	const static statuscode sc_BadRequest;
	const static statuscode sc_InsufficientStorage;

protected:
	string			 _status;
	vector< keyVal > _content;
};

#endif _H_HttpHeader
