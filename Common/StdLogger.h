/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_StdLogger
#define _H_StdLogger
#include <mutex>
#include "ILogger.h"

using std::ostream;
using std::string;
using std::mutex;

/**********************************************************************************************//**
 * \class	StdLogger
 *
 * \brief	ILogger implementation on output streams
 *
 * \author	Friedrich
 * \date	05.09.2015
 **************************************************************************************************/

class StdLogger : public ILogger 
{
public:

	StdLogger(int l = logLevel::warning);
	StdLogger(ostream& pOut, int l = logLevel::warning);
	virtual ~StdLogger();

	virtual void log(int logLevel, const string& logMsg);
	virtual int getLogLevel() const;
	virtual void setLogLevel(int logLevel);
	virtual bool isLogged(int logLevel) const;

protected:
	int				_logLevel;
	mutex			_mutex;
	ostream*		_out;
};

#endif