/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_IProperties
#define _H_IProperties
#include <string>

using std::string;

/**********************************************************************************************//**
 * \class	IProperties
 *
 * \brief	Accessing properties abstraction
 *
 * \author	Friedrich
 * \date	05.09.2015
 **************************************************************************************************/

class IProperties
{
public:	
	virtual ~IProperties() {};

	/**********************************************************************************************//**
	 * \fn	virtual string IProperties::getString(const string& key, const string& default) const = 0;
	 *
	 * \brief	Gets a string from a property key
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	key	   	The property key.
	 * \param	default	The default value, if the property key does not exist.
	 *
	 * \return	The string.
	 **************************************************************************************************/

	virtual string getString(const string& key, const string& default) const = 0;

	/**********************************************************************************************//**
	 * \fn	virtual bool IProperties::setString(const string& key, const string& value) const = 0;
	 *
	 * \brief	Sets a property value for a key
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	key  	The key.
	 * \param	value	The value.
	 *
	 * \return	true if it succeeds, false if it fails.
	 **************************************************************************************************/

	virtual bool setString(const string& key, const string& value) const = 0;
};

/**********************************************************************************************//**
 * \fn	IProperties& getSysProperties();
 *
 * \brief	Gets system properties reference. Static function.
 *
 * \author	Friedrich
 * \date	05.09.2015
 *
 * \return	The system properties.
 **************************************************************************************************/

IProperties& getSysProperties();

#endif