/**********************************************************************************************//**
 * \file	C:\Users\Friedrich\Documents\git\armarc\Common\IRunnable.h
 *
 * \brief	Declares the IRunnable interface.
 **************************************************************************************************/

#pragma once
#ifndef _H_IRunnalbe
#define _H_IRunnalbe

/**********************************************************************************************//**
 * \class	IRunnable
 *
 * \brief	A runnable.
 *
 * \author	Friedrich
 * \date	05.09.2015
 **************************************************************************************************/

class IRunnable
{
public:
	virtual ~IRunnable() {};

	/**********************************************************************************************//**
	 * \fn	virtual void IRunnable::run() = 0;
	 *
	 * \brief	Runs this object.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 **************************************************************************************************/

	virtual void run() = 0;
};

#endif
