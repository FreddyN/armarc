/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_TFactory
#define _H_TFactory
#include <string>
#include <functional>
#include <set>
using std::string;
using std::function;
using std::set;

/**********************************************************************************************//**
 * \class	TFactory
 *
 * \brief	A factory.
 *
 * \author	Friedrich
 * \date	04.09.2015
 *
 * \tparam	T	Generic type parameter.
 **************************************************************************************************/

template<class T>
class TFactory
{
public:
	virtual ~TFactory() {};

	/**********************************************************************************************//**
	 * \fn	virtual T* TFactory::create(const string& cstrName) const = 0;
	 *
	 * \brief	Creates a new instance from the producer, described by cstrName.
	 *
	 * \author	Friedrich
	 * \date	04.09.2015
	 *
	 * \param	cstrName	Name of the cstr.
	 *
	 * \return	null if it fails, else a T*.
	 **************************************************************************************************/

	virtual T* create(const string& cstrName) const = 0;

	/**********************************************************************************************//**
	 * \fn	virtual bool TFactory::addProducer(const string& cstrName, function<T*()>) = 0;
	 *
	 * \brief	Adds a producer allocator function to the factory, and name it with cstrName.
	 *
	 * \author	Friedrich
	 * \date	04.09.2015
	 *
	 * \param	cstrName   	Name of the cstr.
	 * \param [in,out]	()>	If non-null, the ()>
	 *
	 * \return	true if it succeeds, false if it fails.
	 **************************************************************************************************/

	virtual bool addProducer(const string& cstrName, function<T*()>) = 0;	

	/**********************************************************************************************//**
	 * \fn	virtual TFactory::function<T*()> getProducer(const string& cstrName) const = 0;
	 *
	 * \brief	Gets a producer.
	 *
	 * \author	Friedrich
	 * \date	04.09.2015
	 *
	 * \param	cstrName	Name of the cstr.
	 *
	 * \return	null if it the producer name does not exist, else the producer allocator function.
	 **************************************************************************************************/

	virtual function<T*()> getProducer(const string& cstrName) const = 0;

	/**********************************************************************************************//**
	 * \fn	virtual bool TFactory::removeProducer(const string& cstrName) = 0;
	 *
	 * \brief	Removes the producer described by cstrName.
	 *
	 * \author	Friedrich
	 * \date	04.09.2015
	 *
	 * \param	cstrName	Name of the cstr.
	 *
	 * \return	true if it succeeds, false if it fails.
	 **************************************************************************************************/

	virtual bool removeProducer(const string& cstrName) = 0;

	/**********************************************************************************************//**
	 * \fn	virtual void TFactory::getProducerNames(set<string>& producerNames) const = 0;
	 *
	 * \brief	Gets producer names.
	 *
	 * \author	Friedrich
	 * \date	04.09.2015
	 *
	 * \param [in,out]	producerNames	List of names of the producers.
	 **************************************************************************************************/

	virtual void getProducerNames(set<string>& producerNames) const = 0;

	/**********************************************************************************************//**
	 * \fn	virtual void TFactory::extend(const TFactory<T>& src) = 0;
	 *
	 * \brief	Extend the producers from another factory.
	 *
	 * \author	Friedrich
	 * \date	04.09.2015
	 *
	 * \param	src	Source Factory
	 **************************************************************************************************/

	virtual void extend(const TFactory<T>& src) = 0;
};

#endif
