/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <ctime>
#include <iomanip>
#include <sstream>
#include "HttpHeader.h"

using std::put_time;
using std::stringstream;

static auto emptyString = []() { return string(""); };

// General Header fields
const HttpHeader::keyVal HttpHeader::pdhCacheControl =
{ string("Cache-Control"), emptyString };

const HttpHeader::keyVal HttpHeader::pdhConnection =
{ string("Connection"), emptyString };

const HttpHeader::keyVal HttpHeader::pdhConnectionKeepAlive =
{ string("Connection"), []() { return string("keep-alive"); } };

// Response Header 
const HttpHeader::keyVal HttpHeader::pdhDateGMT =
{
	string("Date"),
	[]() { 
		auto t = time(nullptr);
		stringstream sRes;
		sRes << put_time(std::gmtime(&t), "%a, %d-%b-%y %X GMT");
		return sRes.str();
	} 
};

const HttpHeader::keyVal HttpHeader::pdhPragma =
{ string("Pragma"), emptyString };

const HttpHeader::keyVal HttpHeader::pdhContentType =
{ string("Content-Type"), emptyString };

const HttpHeader::keyVal HttpHeader::pdhContentLength =
{ string("Content-Length"), emptyString };

const HttpHeader::keyVal HttpHeader::pdhContentEncodingDeflate =
{ string("Content-Encoding"), []() { return string("deflate"); } };

const HttpHeader::keyVal HttpHeader::pdhSetCookie =
{ string("Set-Cookie"), emptyString };

const HttpHeader::keyVal HttpHeader::pdhExpires =
{ string("Expires"), emptyString };

const HttpHeader::statuscode HttpHeader::sc_OK( 200, "OK" );
const HttpHeader::statuscode HttpHeader::sc_BadRequest(400, "Bad Request");
const HttpHeader::statuscode HttpHeader::sc_NotFound(404, "Not Found");
const HttpHeader::statuscode HttpHeader::sc_Forbidden(403, "Forbidden");
const HttpHeader::statuscode HttpHeader::sc_InternalServerError(500, "Internal Server Error");
const HttpHeader::statuscode HttpHeader::sc_InsufficientStorage(507, "Insufficient Storage");

HttpHeader::HttpHeader()
{

}

HttpHeader::HttpHeader(const statuscode& s, const vector<HttpHeader::keyVal>& items)
{
	stringstream strStatus;
	strStatus << "HTTP/1.1 " << s.val << " " << s.text.c_str() << "\r\n";
	_status = strStatus.str();
	for (auto item : items)
		add(item);
}

HttpHeader::HttpHeader(const statuscode& s)
{
	stringstream strStatus;
	strStatus << "HTTP/1.1 " << s.val << " " << s.text.c_str() << "\r\n";
	_status = strStatus.str();
}

HttpHeader::~HttpHeader()
{
}

void HttpHeader::add(const keyVal& item)
{
	_content.push_back(item);
}

bool HttpHeader::request(const string& key, string& val) const
{
	for (const auto& item : _content) {
		if (item.key.compare(key) == 0) {
			val = item.func();
			return true;
		}
	}
	return false;
}

string HttpHeader::toString() const
{
	string strResult;
	strResult.append(_status);
	for (const auto& item : _content)
		strResult.append( item.key + ": " + item.func() + "\r\n");
	strResult.append("\r\n");
	return strResult;
}

HttpHeader::statuscode::statuscode(int s, const string& hint):
		val(s),
		text(hint)
{
};

HttpHeader::statuscode::statuscode(const statuscode& src)
{
	val = src.val;
	text = src.text;
};
