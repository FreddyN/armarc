/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_ILogger
#define _H_ILogger

#include <string>

/**********************************************************************************************//**
 * \class	ILogger
 *
 * \brief	A logger.
 *
 * \author	Friedrich
 * \date	05.09.2015
 **************************************************************************************************/

class ILogger
{
public:

	/**********************************************************************************************//**
	 * \enum	logLevel
	 *
	 * \brief	Values that represent log levels.
	 **************************************************************************************************/

	enum logLevel { error = 0, warning = 1000, info = 2000, verbose = 3000 };

	/**********************************************************************************************//**
	 * \fn	virtual ~ILogger()
	 *
	 * \brief	Destructor.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 **************************************************************************************************/

	virtual ~ILogger(){};

	/**********************************************************************************************//**
	 * \fn	virtual void log(int, const std::string&) = 0;
	 *
	 * \brief	Logs.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	parameter1	The first parameter.
	 * \param	parameter2	The second parameter.
	 **************************************************************************************************/

	virtual void log(int, const std::string&) = 0;

	/**********************************************************************************************//**
	 * \fn	virtual int getLogLevel() const = 0;
	 *
	 * \brief	Gets log level.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \return	The log level.
	 **************************************************************************************************/

	virtual int getLogLevel() const = 0;

	/**********************************************************************************************//**
	 * \fn	virtual void setLogLevel(int logLevel) = 0;
	 *
	 * \brief	Sets log level.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	logLevel	The log level.
	 **************************************************************************************************/

	virtual void setLogLevel(int logLevel) = 0;

	/**********************************************************************************************//**
	 * \fn	virtual bool isLogged(int logLevel) const = 0;
	 *
	 * \brief	Query if 'logLevel' is logged.
	 *
	 * \author	Friedrich
	 * \date	05.09.2015
	 *
	 * \param	logLevel	The log level.
	 *
	 * \return	true if logged, false if not.
	 **************************************************************************************************/

	virtual bool isLogged(int logLevel) const = 0;
};

#endif
