/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_ModuleLoggerDecorator
#define _H_ModuleLoggerDecorator
#include <string.h>
#include "ILogger.h"

using std::string;

class ModuleLoggerDecorator : public ILogger
{
public:
	ModuleLoggerDecorator(const string& strModule, ILogger* pLogger);
	virtual ~ModuleLoggerDecorator(){};
	virtual void log(int, const string&);
	virtual int getLogLevel() const;
	virtual void setLogLevel(int logLevel);
	virtual bool isLogged(int logLevel) const;

protected:
	string			_strModule;
	ILogger*		_pLog;
};

#endif
