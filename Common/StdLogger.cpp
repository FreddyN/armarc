/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "StdLogger.h"
#include <iostream>

using std::endl;
using std::cout;
using std::ostream;

StdLogger::StdLogger(int l)
	: _logLevel(l) 
{
	_out = &cout;
}

StdLogger::StdLogger(ostream& pOut, int l): 
	_logLevel( l ),
	_out( &pOut )
{	
}

StdLogger::~StdLogger()
{
}

void StdLogger::log(int logLevel, const string& logMsg)
{
	_mutex.lock();
	if (logLevel <= _logLevel)
		*_out << logLevel << ":" << logMsg << endl;
	_mutex.unlock();
}

int StdLogger::getLogLevel() const
{
	return _logLevel;
}

void StdLogger::setLogLevel(int logLevel)
{
	_logLevel = logLevel;
}

bool StdLogger::isLogged(int logLevel) const
{
	if (logLevel <= _logLevel)
		return true;
	else
		return false;
}
