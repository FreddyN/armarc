/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <string>
#include "ModuleLoggerDecorator.h"

using std::string;

ModuleLoggerDecorator::ModuleLoggerDecorator(const string& strModule, ILogger* pLogger) :
	_strModule(strModule),
	_pLog(pLogger)
{
}

void ModuleLoggerDecorator::log(int loglevel, const string& strLog)
{	
	_pLog->log(loglevel, string(_strModule) + ":" + strLog);
}

int ModuleLoggerDecorator::getLogLevel() const
{
	return _pLog->getLogLevel();
}

void ModuleLoggerDecorator::setLogLevel(int logLevel)
{
	_pLog->setLogLevel(logLevel);
}

bool ModuleLoggerDecorator::isLogged(int logLevel) const
{
	return _pLog->isLogged( logLevel );
}
