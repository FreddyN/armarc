/**
 * Created by Simon on 25.07.2015.
 */

armaGpsApp.controller('MainNavigationController', ['$scope', '$http', function ($scope, $http) {

    /*
     please insert your server configuration here
     */
    $scope.canvas = null;
    $scope.canvasContext = null;
    $scope.canvasBackgroundImage = null;

    // sizes of player in canvas
    $scope.playerRadius = 10;
    $scope.offsetX = $scope.playerRadius / 2;
    $scope.offsetY = $scope.playerRadius / 2;

    $scope.touchMoveFactor = 0.5;

    $scope.serverConfig = {
        "port": 9192,
        "host": "./",
        "protocol": 'http://'
    };

    $scope.touchPosition = {
        "x": 0,
        "y": 0
    }

    $scope.mapViewOptions = {
        "offset": {
            "top": 0,
            "left": 0
        },
        // modes can be free or playerFixed
        "mode": "free",
        "zoomFactor": 1.0
    };

    $scope.scaleFactor = 1;

    $scope.mapConfiguration = {
        "width": null,
        "height": null,
        "imgfile": null,
        "Welcome": null
    };

    $scope.pause = true;
    $scope.gameInterval = null;
    $scope.mapMarkerInterval = null;
    $scope.mapInterval = null;

    $scope.welcomeMessage = 'Welcome to : ';
    $scope.armaPlayers = new Array();
    $scope.mapMarkers = new Array();
    $scope.mapEmpty = new Array();
    $scope.errorMessage = null;
    $scope.playerFocusID = -1;

    /**
     * binding the scroll event
     * @param $event
     * @param delta
     * @param deltax
     * @param deltay
     */
    $scope.scroll = function ($event, delta, deltax, deltay) {
        console.log($event, delta, deltax, deltay);
    };

    /**
     * this functions polls new data from the webserver
     * to be sure that the data is not corrupt please
     * DO NOT poll faster than every 1/4 sec.
     */
    $scope.getAllPlayerData = function () {
        var url = '/api/getct?key=AllPlayers';
        $http.get(url).
        success(function (data) {
            $scope.clearPlayerData();
            for (var i = 0; i < data.names.length; i++) {
                $scope.createPlayerDataFromJson(
                    data.names[i],
                    data.positions[i],
                    data.alive[i],
                    data.dir[i],
                    data.side[i],
                    data.speed[i],
                    data.tickcount
                );
            }
            $scope.errorMessage = null;
        }).
        error(function (data, status) {
            $scope.errorMessage = data + status;
            return;
        });
    };

    /**
     * this functions polls new data from the webserver
     * to be sure that the data is not corrupt please
     * DO NOT poll faster than every 1/4 sec.
     */
    $scope.getMapMarkers = function () {
        var url = '/api/getct?key=MapMarkers';
        $http.get(url).
        success(function (data) {
            $scope.clearMapMarkers();
            for (var i = 0; i < data.names.length; i++) {
                if (( data.types[i] === "hd_dot")) {
                    $scope.createMarkerFromJson(
                        data.types[i],
                        data.sizes[i],
                        data.pos[i],
                        data.dir[i],
                        data.colors[i],
                        data.text[i]
                    );
                }
            }
            $scope.errorMessage = null;
        }).
        error(function (data, status) {
            $scope.errorMessage = data + status;
            return;
        });
        console.log("new map markers received");
    };


    /**
     * this functions loads initially the map configuration
     */
    $scope.getMapConfiguration = function () {
        var url = '/api/getct?key=MapConf';
        $http.get(url).
        success(function (data) {
            $scope.mapConfiguration = data;
            $scope.welcomeMessage = data.Welcome;
            console.log("Map conf: " + $scope.mapConfiguration.width);
            $scope.initCanvas();
        }).
        error(function (data, status) {
            return;
        });

    };

    /**
     * create a player object from data
     * @param playerName
     * @param positions
     * @param isAlive
     * @param direction
     * @param team
     * @param speed
     * @param tickCount
     */
    $scope.createPlayerDataFromJson = function (playerName, positions, isAlive, direction, team, speed, tickCount) {

        /* create new player object from the params */
        var newPlayerData = {
            "name": playerName,
            "position": {
                "x": positions[0],
                "y": positions[1],
                "z": positions[2]
            },
            "isAlive": isAlive,
            "direction": direction,
            "team": team,
            "speed": speed,
            "tickCount": tickCount
        };
        $scope.armaPlayers.push(newPlayerData);
    };


    /**
     * this void function clears all received player data
     */
    $scope.clearPlayerData = function () {
        $scope.armaPlayers = [];
    };

    /**
     * this void function clears all map marker data
     */
    $scope.clearMapMarkers = function () {
        $scope.mapMarkers = [];
    };


    /**
     * creates a marker object
     * @param type
     * @param size
     * @param pos
     * @param direction
     * @param color
     * @param text
     */
    $scope.createMarkerFromJson = function (type, size, pos, direction, color, text) {
        var newMarker = {
            "type": type,
            "size": {
                "x": size[0],
                "y": size[1]
            },
            "position": {
                "x": pos[0],
                "y": pos[1]
            },
            "direction": direction,
            "color": color,
            "text": text
        };
        $scope.mapMarkers.push(newMarker);
    };

    /**
     * send marker data to server
     * @param marker
     */
    $scope.sendMarker = function (marker) {

    };

    /**
     * pause the polling for data
     */
    $scope.togglePause = function () {
        if ($scope.pause === true) {
            $scope.pause = false;
            console.log("receiving data ...");
            $scope.gameInterval = setInterval($scope.getAllPlayerData, 250);
            $scope.mapMarkerInterval = setInterval($scope.getMapMarkers, 250);
            $scope.mapInterval = setInterval($scope.getMapConfiguration, 1000*60);

        } else {
            clearInterval($scope.gameInterval);
            clearInterval($scope.mapMarkerInterval);
            clearInterval($scope.mapInterval);
            console.log("Stop receiving data ...");
            $scope.pause = true;
        }
    };

    /**
     * init function
     */
    $scope.init = function () {
        $scope.getMapConfiguration();
        $scope.togglePause();
    };


    /**
     * this functions is used to translate a position relative
     * to the scaleFactor
     * @param posX
     * @returns {number}
     */
    $scope.translatePositionX = function (posX) {
        return posX * $scope.scaleFactor;
    };

    /**
     * this function is used to translate a position relative
     * to the scaleFactor
     * @param posY
     * @returns {number}
     */
    $scope.translatePositionY = function (posY) {
        return posY * $scope.scaleFactor;
    };

    /**
     *
     */
    $scope.renderMap = function () {
        $scope.canvasContext.clearRect(0, 0, $scope.canvas.width, $scope.canvas.height);
        $scope.canvasContext.globalAlpha = 1.0;

        // first draw background
        $scope.canvasDrawImage();

        $scope.drawMapMarkers();
        $scope.drawPlayers();
        //$scope.centerWindowToPlayer();
    };

    $scope.centerWindowToPlayer = function () {
        if ($scope.playerFocusID >= 0) {
            var p = $scope.armaPlayers[$scope.playerFocusID];
            var px = $scope.translatePositionX(p.position.x) + offsetX;
            var py = $scope.translatePositionY(p.position.y) + offsetY;
            var center_w = px - window.innerWidth / 2;
            var center_h = py - window.innerHeight / 2;
            window.scroll(center_w, center_h);
        }
    };

    $scope.drawPlayers = function () {
        $scope.canvasContext.strokeStyle = 'red';
        $scope.canvasContext.lineWidth = 2;
        var text_offsetx = 12;
        var text_offsety = 6;
        $scope.canvasContext.font = "status-bar";

        // create radial gradient       
        for (var i = 0; i < $scope.armaPlayers.length; i++) {
            var playerPositionX = ($scope.translatePositionX($scope.armaPlayers[i].position.x) + $scope.offsetX - $scope.mapViewOptions.offset.left);
            var playerPositionY = ($scope.translatePositionY($scope.armaPlayers[i].position.y) + $scope.offsetY  - $scope.mapViewOptions.offset.top);
            var playerGradient = $scope.canvasContext.createRadialGradient(playerPositionX, playerPositionY, 0, playerPositionX, playerPositionY, $scope.playerRadius);
            playerGradient.addColorStop(0, "#ff00ff");
            playerGradient.addColorStop(1, "#004000");
            $scope.canvasContext.fillStyle = playerGradient;
            $scope.canvasContext.beginPath();
            $scope.canvasContext.arc(playerPositionX, playerPositionY, $scope.playerRadius, 0, 2 * Math.PI);
            $scope.canvasContext.closePath();
            $scope.canvasContext.fill();
        }
        $scope.canvasContext.fillStyle = "#000000";
        $scope.canvasContext.font = "status-bar";

        for (var i = 0; i < $scope.armaPlayers.length; i++) {
            var playerPositionX = ($scope.translatePositionX($scope.armaPlayers[i].position.x) + $scope.offsetX - $scope.mapViewOptions.offset.left);
            var playerPositionY = ($scope.translatePositionY($scope.armaPlayers[i].position.y) + $scope.offsetY - $scope.mapViewOptions.offset.top);
            if (i !== $scope.playerFocusID)
                $scope.canvasContext.fillText($scope.armaPlayers[i].name, playerPositionX + text_offsetx, playerPositionY + text_offsety);
        }

        if ($scope.playerFocusID >= 0) {
            $scope.canvasContext.fillStyle = "#ff0000";
            $scope.canvasContext.font = "caption";
            var playerPositionX = $scope.translatePositionX($scope.armaPlayers[$scope.playerFocusID].position.x) + $scope.offsetX;
            var playerPositionY = $scope.translatePositionY($scope.armaPlayers[$scope.playerFocusID].position.y) + $scope.offsetY;
            $scope.canvasContext.fillText($scope.armaPlayers[$scope.playerFocusID].name, playerPositionX + text_offsetx, playerPositionY + text_offsety);
        }
    };

    $scope.drawMapMarkers = function () {
        for (var i = 0; i < $scope.mapMarkers.length; i++) {
            var px = ($scope.translatePositionX($scope.mapMarkers[i].position.x) - $scope.mapViewOptions.offset.left);
            var py = ($scope.translatePositionY($scope.mapMarkers[i].position.y) - $scope.mapViewOptions.offset.top);
            switch ($scope.mapMarkers[i].type) {
                case 'hd_dot':
                    $scope.draw_hd_dot(px, py, $scope.mapMarkers[i].text);
                    break;
                default:
                    $scope.canvasContext.fillText($scope.mapMarkers[i].text, px + 10, py);
                    break;
            }
        }
    };

    /**
     * this functions is used to show hd_dot markers
     * to the scaleFactor
     * @param posX
     * @param posY
     * @param text
     */
    $scope.draw_hd_dot = function (posX, posY, text) {
        $scope.canvasContext.strokeStyle = 'red';
        $scope.canvasContext.fillStyle = 'black';
        $scope.canvasContext.font = "18px Arial";
        $scope.canvasContext.lineWidth = 2;
        $scope.canvasContext.beginPath();
        $scope.canvasContext.arc(posX, posY, 6, 0, 2 * Math.PI);
        $scope.canvasContext.stroke();
        $scope.canvasContext.fill();
        $scope.canvasContext.fillText(text, posX + 10, posY + 6);
    };

    /**
     *
     * @param mousePositionX
     * @param mousePositionY
     * @param radius
     * @returns {number}
     */
    $scope.findNearestPlayer = function (mousePositionX, mousePositionY, radius) {
        var max_distance = 1.0e37;
        var player_id = -1;
        for (var i = 0; i < $scope.armaPlayers.length; i++) {
            var px = $scope.translatePositionX($scope.armaPlayers[i].position.x) + offsetX;
            var py = $scope.translatePositionY($scope.armaPlayers[i].position.y) + offsetY;
            var distance = Math.sqrt((px - mousePositionX) * (px - mousePositionX) + (py - mousePositionY) * (py - mousePositionY));
            if (distance < max_distance) {
                max_distance = distance;
                player_id = i;
            }
        }
        if (max_distance <= radius) {
            return player_id;
        }
        return -1;
    };

    /**
     *
     * @param event
     * @constructor
     */
    $scope.OnClick = function (event) {
        var mx = event.offsetX;
        var my = event.offsetY;
        var playerID = $scope.findNearestPlayer(mx, my, $scope.playerRadius);
        if (playerID >= 0)
            $scope.playerFocusID = playerID;
        else
            $scope.playerFocusID = -1;
    };

    /**
     *
     * @param event
     */
    $scope.onMouseButtonPress = function (event) {
        event.preventDefault();
        $scope.touchPosition.x = event.pageX;
        $scope.touchPosition.y = event.pageY;

//        var playerid = $scope.findNearestPlayer(touchPositionX, touchPositionY, $scope.playerRadius * 10);
//        if ( playerid >= 0 )
//            $scope.playerFocusID = playerid;
//        else
//            $scope.playerFocusID = -1;
    };

    $scope.onMouseButtonRelease = function (event) {
        event.preventDefault();
        var onTouchEndPositionX = event.pageX;
        var onTouchEndPositionY = event.pageY;

        var vectorMovedX = $scope.touchPosition.x - onTouchEndPositionX;
        var vectorMovedY = $scope.touchPosition.y - onTouchEndPositionY;

        $scope.touchPosition.x = 0;
        $scope.touchPosition.y = 0;

        $scope.moveMap($scope.mapViewOptions.offset.left + vectorMovedX, $scope.mapViewOptions.offset.top + vectorMovedY);
    };

    $scope.onTouchStart = function (event) {
            $scope.touchPosition.x = event.targetTouches[0].pageX;
            $scope.touchPosition.y = event.targetTouches[0].pageY;

    };

    $scope.onTouchEnd = function (event) {
        var onTouchEndPositionX = event.targetTouches[0].pageX;
        var onTouchEndPositionY = event.targetTouches[0].pageY;

        var vectorMovedX = ($scope.touchPosition.x - onTouchEndPositionX) * $scope.touchMoveFactor;
        var vectorMovedY = ($scope.touchPosition.y - onTouchEndPositionY) * $scope.touchMoveFactor;

        $scope.touchPosition.x = 0;
        $scope.touchPosition.y = 0;

        $scope.moveMap($scope.mapViewOptions.offset.left + vectorMovedX, $scope.mapViewOptions.offset.top + vectorMovedY);
    };

    $scope.resizeCanvas = function () {
        $scope.canvas.height = window.innerHeight;
        $scope.canvas.width = window.innerWidth;
    };

    /**
     *
     */
    $scope.initCanvas = function () {
        // TODO: replace with angular stuff
        $scope.canvas = document.getElementById('mapImage');
        $scope.canvasContext = $scope.canvas.getContext('2d');
        $scope.resizeCanvas();
        console.log("Canvas object initialized:");

        // read image from server and put into canvas
        try {
            $scope.canvasBackgroundImage = new Image();
            console.log("Canvas image object initialized")
        } catch (errorEvent) {
            console.log("Error could not initialize image object" + errorEvent);
        }

        $scope.canvasBackgroundImage.src = $scope.mapConfiguration.imgfile;

        // populate events for canvas
        $scope.canvas.addEventListener('mousedown', $scope.onMouseButtonPress, false);
        $scope.canvas.addEventListener('mouseup', $scope.onMouseButtonRelease, false);

        $scope.canvas.addEventListener('touchstart', $scope.onTouchStart, false);
        $scope.canvas.addEventListener('touchend', $scope.onTouchEnd, false);

        $scope.drawInterval = setInterval($scope.renderMap, 1000 / 60);

    };

    $scope.canvasDrawImage = function () {
        //console.log("Trying to draw image on canvas");
        var imagePath = $scope.canvasBackgroundImage.src;
        //console.log("Imagepath given: " + imagePath);
        if ($scope.mapViewOptions.mode != "playerFixed") {
            $scope.canvasContext.drawImage(
                $scope.canvasBackgroundImage,
                $scope.mapViewOptions.offset.left / $scope.mapViewOptions.zoomFactor,
                $scope.mapViewOptions.offset.top /  $scope.mapViewOptions.zoomFactor,
                $scope.mapConfiguration.width  / $scope.mapViewOptions.zoomFactor,
                $scope.mapConfiguration.height / $scope.mapViewOptions.zoomFactor,
                0,
                0,
                $scope.canvasBackgroundImage.width,
                $scope.canvasBackgroundImage.width
            );
        }

    };

    /**
     *
     * @param offsetTop
     * @param offsetLeft
     */
    $scope.moveMap = function (offsetLeft, offsetTop) {
        $scope.mapViewOptions.offset.top = offsetTop;
        $scope.mapViewOptions.offset.left = offsetLeft;
    };

    $scope.init();
    //$scope.canvas.addEventListener('click', $scope.OnClick, false);

    window.addEventListener("resize", $scope.resizeCanvas);
}]);
