/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <string>
#include <iostream>
#include "StdLogger.h"
#include "IProperties.h"
#include "PluginFactory.h"
#include "WriteJsonCachePluginTask.h"
#include "FetchValuePluginTask.h"
#include "GetValuePluginTask.h"
#include "ExamplePluginTask.h"
#include "MergeJsonCachePluginTask.h"

using std::wstring;
using std::string;
using std::cerr;

static BOOL		dll_initialized = FALSE;
static const	char g_cstrArmaHTTPExe[] = "ArmaHTTP.exe";
static wstring	g_cwstrArmaPath;
static wstring	g_cwstrArmaExec;
PluginFactory	g_PluginFactory;

StdLogger		g_StdOutLogger;
ILogger*		stdlog = &g_StdOutLogger;

void startArmaHTTPProcess(const string& cstrWorkingPath, const string& cstrPath)
{
	PROCESS_INFORMATION procInfo = { 0 };
	STARTUPINFO startupInfo = { 0 };
	SECURITY_ATTRIBUTES saAttr = { 0 };
	saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
	startupInfo.cb = sizeof(STARTUPINFO);
	startupInfo.dwFlags = STARTF_USESHOWWINDOW;
	startupInfo.wShowWindow = SW_HIDE;
	wstring strProcess(cstrPath.begin(), cstrPath.end());
	wstring strProcessWorkingDir(cstrWorkingPath.begin(), cstrWorkingPath.end());
	g_cwstrArmaPath = strProcessWorkingDir;
	g_cwstrArmaExec = strProcess;
	BOOL result = CreateProcess(
		strProcess.c_str(),
		NULL,
		&saAttr,
		NULL,
		FALSE,
		NORMAL_PRIORITY_CLASS,
		NULL,
		strProcessWorkingDir.c_str(),
		&startupInfo,
		&procInfo
	);	
	if (!result)
		cerr << "WTF! ArmaHTTP Service can not start.";
	else
	{
		// 1sec delay time to setup the process	
		Sleep(2500);
	}
};

void InitPluginFactory(PluginFactory& pf, ILogger* pLogger)
{
	pf.addProducer("WriteJsonCache", [&pLogger]() { return (GenericPluginTask*) new WriteJsonCachePluginTask(pLogger); });	
	pf.addProducer("MergeJsonCache", [&pLogger]() { return (GenericPluginTask*) new MergeJsonCachePluginTask(pLogger); });
	pf.addProducer("FetchValue", [&pLogger]() { return (GenericPluginTask*) new FetchValuePluginTask(pLogger); });
	pf.addProducer("GetValue", [&pLogger]() { return (GenericPluginTask*) new GetValuePluginTask(pLogger); });
	pf.addProducer("ping", [&pLogger]() { return (GenericPluginTask*) new ExamplePluginTask(pLogger); });
}

BOOL InitializeArmaGPSDll()
{
	const string strInstalledDir =
		getSysProperties().getString(
			"InstallationDir"
			, "C:\\Program Files (x86)\\Banana Squad\\ArmaRC\\" // Default installation path
			);

	if (strInstalledDir.length())
	{
		const string cstrArmaHTTP = strInstalledDir + string(g_cstrArmaHTTPExe);
		startArmaHTTPProcess(strInstalledDir, cstrArmaHTTP);
		dll_initialized = TRUE;
	}

	InitPluginFactory( g_PluginFactory, stdlog);
	return dll_initialized;
}

void DeInitializeArmaGPSDll()
{
	dll_initialized = false;
}

// DllMain() is the entry-point function for this DLL. 
BOOL WINAPI DllMain(HINSTANCE hinstDLL, // DLL module handle
	DWORD fdwReason,                    // reason called
	LPVOID lpvReserved)                 // reserved
{
	switch (fdwReason)
	{
		// The DLL is loading due to process 
		// initialization or a call to LoadLibrary. 

	case DLL_PROCESS_ATTACH:
		// Initiate ArmaHTTP, when loading the DLL
		if (!dll_initialized)
			return InitializeArmaGPSDll();
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		if (dll_initialized)
			DeInitializeArmaGPSDll();
		break;
	default:
		break;
	}

	return TRUE;
}

