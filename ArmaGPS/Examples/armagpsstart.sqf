ArmaGPS_Map = _this select 0;
ArmaGPS_MapMaxX = _this select 1;
ArmaGPS_MapMaxY = _this select 2;
ArmaGPS_MapImgX = _this select 3;
ArmaGPS_MapImgY = _this select 4;
ArmaGPS_MapImgEffX = _this select 5;
ArmaGPS_MapImgEffY = _this select 6;
ArmaGPS_MapNulX = _this select 7;
ArmaGPS_MapNulY = _this select 8;
ArmaGPS_WelcomeMsg = _this select 9;

ArmaGPS_MapScaleX = ArmaGPS_MapImgEffX / ArmaGPS_MapMaxX;
ArmaGPS_MapScaleY = ArmaGPS_MapImgEffY / ArmaGPS_MapMaxY;

ArmaGPS_Ext = "ArmaGPS";

ArmaGPS_GetUnitPosArr = {
	private["_allUnitPos","_allUnits","_curPos1","_curPos2"];	
	_allUnitPos = [];		
	_allUnits = _this select 0;
	{	_curPos1 = position _x;
		_curPos2 = [];
		_curPos2 pushBack( (_curPos1 select 0) * ArmaGPS_MapScaleX + ArmaGPS_MapNulX );
		_curPos2 pushBack( ArmaGPS_MapImgY - ((_curPos1 select 1) * ArmaGPS_MapScaleY) + ArmaGPS_MapNulY );
		_curPos2 pushBack( _curPos1 select 2 );
		_allUnitPos pushBack ( _curPos2 );
	} forEach _allUnits;	
	_allUnitPos;
};

ArmaGPS_GetUnitNameArr = {
	private["_allUnitNames","_allUnits"];	
	_allUnitNames = [];
	_allUnits = _this select 0;
	{
		_allUnitNames pushBack ( name _x );
	} forEach _allUnits;
	_allUnitNames;
};

ArmaGPS_GetUnitSideArr = {
	private["_allUnitNames","_allUnits"];	
	_allUnitNames = [];
	_allUnits = _this select 0;
	{
		_allUnitNames pushBack( format ["%1", side _x] );
	} forEach _allUnits;
	_allUnitNames;
};

ArmaGPS_GetUnitAliveArr = {
	private["_allUnitNames","_allUnits"];	
	_allUnitNames = [];
	_allUnits = _this select 0;
	{
		_allUnitNames pushBack ( alive _x );
	} forEach _allUnits;
	_allUnitNames;
};

ArmaGPS_GetUnitDirArr = {
	private["_allUnitNames","_allUnits"];	
	_allUnitNames = [];
	_allUnits = _this select 0;
	{
		_allUnitNames pushBack ( direction _x );
	} forEach _allUnits;
	_allUnitNames;
};

ArmaGPS_GetUnitSpeedArr = {
	private["_allUnitSpeed","_allUnits"];	
	_allUnitSpeed= [];
	_allUnits = _this select 0;
	{
		_allUnitSpeed pushBack ( speed _x );
	} forEach _allUnits;
	_allUnitSpeed;
};

ArmaGPS_GetPlayerUIDArr = {
	private["_allUnitUID","_allUnits","_uidtag"];	
	_allUnitUID = [];
	_allUnits = _this select 0;
	{
		_uidtag = format [ "%1", getPlayerUID _x ];
		_allUnitUID pushBack ( _uidtag );
	} forEach _allUnits;
	_allUnitUID;
};

ArmaGPS_GetMarkerName = {
    private "_unit";
    _unit = _this select 0;

    if (getNumber(configFile >> "CfgVehicles" >> typeOf(_unit) >> "attendant") == 1) exitWith
    {
        "Medic";
    };

    if (getNumber(configFile >> "CfgVehicles" >> typeOf(_unit) >> "engineer") == 1) exitWith
    {
        "Engineer";
    };

    if (leader(_unit) == _unit) exitWith
    {
        "Leader";
    };

    if (getText(configFile >> "CfgWeapons" >> primaryWeapon(_unit) >> "UIPicture") == "\a3\weapons_f\data\ui\icon_mg_ca.paa") exitWith
    {
        "MG";
    };

    if (getText(configFile >> "CfgWeapons" >> secondaryWeapon(_unit) >> "UIPicture") == "\a3\weapons_f\data\ui\icon_at_ca.paa") exitWith
    {
        "AT";
    };

    "Rifleman";
};

ArmaGPS_GetUnitWeaponClassArr = {
	private["_allUnitWeapon","_allUnits","_weaponclass"];	
	_allUnitWeapon = [];
	_allUnits = _this select 0;
	{	_weaponclass = [_x] call ArmaGPS_GetMarkerName;
		_allUnitWeapon pushBack (_weaponclass);
	} forEach _allUnits;
	_allUnitWeapon;
};

ArmaGPS_UnitsToJSON = {
	private["_allUnits", "_names", "_positions" ,"_alive", "_dir", "_sides", "_speed", "_weaponclasses", "_tickcount"];
	_allUnits = _this select 0;
	_names = [_allUnits] call ArmaGPS_GetUnitNameArr;
	_positions = [_allUnits] call ArmaGPS_GetUnitPosArr;
	
	_alive = [_allUnits] call ArmaGPS_GetUnitAliveArr;
	_dir = [_allUnits] call ArmaGPS_GetUnitDirArr;
	_sides = [_allUnits] call ArmaGPS_GetUnitSideArr;
	_speed = [_allUnits] call ArmaGPS_GetUnitSpeedArr;	
	_weaponclasses = [_allUnits] call ArmaGPS_GetUnitWeaponClassArr;
	
	_tickcount = time * 1000;
	_return = format [ "{ ""numPlayers"": %1, ""names"": %2, ""positions"": %3, ""alive"": %4, ""dir"": %5, ""side"": %6, ""speed"": %7, "
						, count _allUnits, _names, _positions, _alive, _dir, _sides, _speed
					  ];	
	_return = _return + format [ """weaponClasses"": %1,""tickcount"": %2", _weaponclasses, _tickcount ];
	_return = _return + " }";	
	_return;
};


ArmaGPS_MarkersToJSON = {
	private[ "_names","_pos","_colors","_types","_dir","_return", "_curPos1","_curPos2", "_curSize1","_curSize2" ];
	_names = [];
	_pos = [];
	_colors = [];
	_sizes = [];
	_types = [];
	_text = [];
	_dir = [];
	{	_curPos1 = getMarkerPos _x;
		_curPos2 = [];
		_curPos2 pushBack( (_curPos1 select 0) * ArmaGPS_MapScaleX + ArmaGPS_MapNulX );
		_curPos2 pushBack( ArmaGPS_MapImgY - ((_curPos1 select 1) * ArmaGPS_MapScaleY) + ArmaGPS_MapNulY );
		_curPos2 pushBack( _curPos1 select 2 );

		_curSize1 = getMarkerSize _x;
		_curSize2 = [];
		_curSize2 pushBack( (_curSize1 select 0) * ArmaGPS_MapScaleX );
		_curSize2 pushBack( (_curSize1 select 1) * ArmaGPS_MapScaleY );
		
		_names pushBack _x ;
		_pos pushBack ( _curPos2 );
		_sizes pushBack ( _curSize2 );
		_types pushBack ( getMarkerType _x );	
		_colors pushBack ( getMarkerColor _x );
		_text pushBack ( markerText _x );		
		_dir pushBack ( markerDir _x);
	} forEach allMapMarkers;
	_return = format [ "{ ""names"": %1, ""pos"": %2, ""sizes"": %3, ""types"": %4, ""colors"": %5, ""text"": %6, ""dir"": %7, ""daytime"": %8, ""date"": %9, "
						, _names, _pos, _sizes, _types, _colors, _text, _dir, daytime, date
					];
	_return = _return + format [ """map"": ""%1"" }", ArmaGPS_Map ];
	_return;
};

ArmaGPS_InitMap =
{
	private["_pluginCall","_JSONMapConf","_preturn","_retArray","_URL"];
	_JSONMapConf = format [ "{ ""width"": %1, ""height"": %2, ""imgfile"": ""%3"", ""Welcome"": ""%4"" }"
							, ArmaGPS_MapImgX, ArmaGPS_MapImgY, ArmaGPS_Map, ArmaGPS_WelcomeMsg
						   ];
	_pluginCall = "d:WriteJsonCache MapConf " + _JSONMapConf;
	ArmaGPS_Ext callExtension _pluginCall;
	
	_pluginCall = "d:GetValue ArmaRC armarcurl";
	_preturn = ArmaGPS_DLLToTest callExtension _pluginCall;	
	if ( count (toArray _preturn) > 0 ) then
	{
		_retArray = call compile _preturn;
		_URL = _retArray select 0;		
		hint format ["ArmaGPS available"];
	}
	else
	{
		hint "ArmaRC is down, or key ""armarcurl"" is not configured.";		
	}
};

ArmaGPS_PerformSIDContent =
{
	private ["_sid","_cmd","_uid","_trustlevel"];
	_sid = _this select 0;
	_uid = _this select 1;
	_trustlevel = _this select 2;
	if ( _trustlevel > 0) then
	{
		_cmd = [] call compile (_this select 3);
		switch (_cmd select 0) do
		{
			case "say":
			{
				systemChat format["%1 say:%2", _uid, _cmd select 1];
			};
			default
			{
				systemChat format["%1 want %2, not implemented", _sid, _cmd];
			};
		}
	};
	if ( _trustlevel > 1) then
	{		
		switch (_cmd select 0) do
		{
			case "say":
			{
				systemChat format["%1 say:%2", _uid, _cmd select 1];
			};
			default
			{
				systemChat format["%1 want %2, not implemented", _sid, _cmd];
			};
		}
	};
	
};

ArmaGPS_PollingSIDContent = 
{
	private ["_httpsids","_pluginCall","_preturn"];
	_httpsids = [];
	_httpsids = _this select 0;
	{		
		if ( (_x select 2) > 0 ) then
		{
			_pluginCall = format ["d:FetchValue %1 cmd", _x select 0];
			_preturn = ArmaGPS_DLLToTest callExtension _pluginCall;
			switch( _preturn ) do
			{
				case "ERROR":
				{				
				};
				case "[]":
				{					
				};
				default
				{
					_nul = [_x select 0, _x select 1, _x select 2, _preturn] call ArmaGPS_PerformSIDContent;
				};
			}
		};
	} forEach _httpsids;
};

ArmaGPS_PollingSIDs =
{		
	ArmaHTTP_Sessions = [];
	ArmaHTTP_SIDs = [];
	[] spawn
	{
		private["_pluginCall","_preturn","_newSessionsFunc","_newSessions"];
		_pluginCall = "d:GetValue SIDs sessions";
		while {true} do
		{
			_preturn = ArmaGPS_DLLToTest callExtension _pluginCall;
			switch (_preturn) do
			{
				case "ERROR":
				{
					systemChat "Mailfunction on ArmaHTTP";
				};
				case "[]":
				{
					systemChat "Nobody is logged in ArmaHTTP";
				};
				default
				{		
						ArmaHTTP_Sessions = [] call compile _preturn;
						_nul = [ArmaHTTP_Sessions] call ArmaGPS_PollingSIDContent;
				};				
			};
			sleep 2.5;
		}
	}
};


ArmaGPS_InitPlayableUnits =
{
    [] spawn
    {
		private["_allPlayerPosJson", "_pluginCall"];
		while {true} do
        {	
			_visibleUnits = playableUnits + allPlayers;
			_allPlayerPosJson = [_visibleUnits] call ArmaGPS_UnitsToJSON;
			_pluginCall = "d:WriteJsonCache AllPlayers " + _allPlayerPosJson;
			ArmaGPS_Ext callExtension _pluginCall;		
			sleep 0.25;
        }
    }
};

ArmaGPS_InitMarkers =
{
    [] spawn
    {
		private["_allMarkersToJson", "_pluginCall"];
		while {true} do		
        {				
			_allMarkersToJson = [] call ArmaGPS_MarkersToJSON;
			_pluginCall = "d:WriteJsonCache MapMarkers " + _allMarkersToJson;
			ArmaGPS_Ext callExtension _pluginCall;		
			sleep 2.0;
        };		
    };	
};

ArmaGPS_Init = 
{
	private["_pluginCall", "_null" ];		
	_null = [] call ArmaGPS_InitMap;
	_null = [] call ArmaGPS_InitPlayableUnits;
	_null = [] call ArmaGPS_InitMarkers;
	_null = [] call ArmaGPS_PollingSIDs;
	systemChat format["Starting ArmaGPS on %1", ArmaGPS_Map];	
};

[] call ArmaGPS_Init;
