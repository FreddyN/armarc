comment "SQF Component Tests for ArmaHTTP and ArmaGPS.dll";
private ["_return"];
ArmaGPS_DLLToTest = "ArmaGPS";

comment "'WriteJsonCache' are useful to set up JSON container for the ArmaHTTP process";
comment "Intention: SQF -> HTML";
ArmaGPSUT_WriteJsonCache = 
{
	private["_return","_pluginCall"];
	
	_pluginCall = "d:WriteJsonCache TestContainer { ""key"" : ""value"" }";
	_return = ArmaGPS_DLLToTest callExtension _pluginCall;	
	if ( _return != "OK" )
		exitWith{"Test ArmaGPSUT_WriteJsonCache FAILED"};
	"Test ArmaGPSUT_WriteJsonCache OK.";
};

comment "'MergeJsonCache' is useful to add Keys/Values to an existing JSON container for the ArmaHTTP process";
comment "Intention: SQF -> HTML";
ArmaGPSUT_MergeJsonCache = 
{
	private["_return","_pluginCall","_preturn","_testArray","_testVal"];

comment "Create a container";	
	_pluginCall = "d:WriteJsonCache TestContainer { ""key1"" : ""value1"" }";	
	ArmaGPS_DLLToTest callExtension _pluginCall;
	
comment "Merge additional data into the container";
	_pluginCall = "d:MergeJsonCache TestContainer { ""key2"" : ""value2"" }";
	ArmaGPS_DLLToTest callExtension _pluginCall;
	
comment "Check merge result for 'key1'";
	_pluginCall = "d:GetValue TestContainer key1";
	_preturn = ArmaGPS_DLLToTest callExtension _pluginCall;
	_testArray = call compile _preturn;
	_testVal = _testArray select 0;
	if ( _testVal != "value1" )
		exitWith{"Test ArmaGPSUT_MergeJsonCache FAILED1"};	
	
comment "Check merge result for 'key2'";
	_pluginCall = "d:GetValue TestContainer key2";
	_preturn = ArmaGPS_DLLToTest callExtension _pluginCall;
	_testArray = call compile _preturn;
	_testVal = _testArray select 0;
	if ( _testVal != "value2" )
		exitWith{"Test ArmaGPSUT_MergeJsonCache FAILED2"};	
		
	"Test ArmaGPSUT_MergeJsonCache OK.";
};

_return = [] call ArmaGPSUT_WriteJsonCache;
systemChat _return;

_return = [] call ArmaGPSUT_MergeJsonCache;
systemChat _return;
