/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_GenericPluginTask
#define _H_GenericPluginTask

#include <iostream>
#include <string>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <atomic>
#include <queue>
#include <ctime>
#include "IRunnable.h"

using std::string;
using std::atomic;
using std::time_t;
using std::nullptr_t;


struct GenericPluginData
{
	atomic<bool>  ready = false;
	string params = "";
	string result = "";
	time_t time = std::time(nullptr);
	time_t ttl = 60 * 60 * 24;				// Default TTL 24 hours
};

class GenericPluginTask : public IRunnable
{
public:
	GenericPluginTask()
	{
	};
	virtual ~GenericPluginTask(){  };
	virtual void run() {
	};
	GenericPluginData& getData()
	{ 
		return _data; 
	};

protected:
	GenericPluginData _data;	
};

#endif
