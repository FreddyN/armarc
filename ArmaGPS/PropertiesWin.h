/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_PropertiesWin
#define _H_PropertiesWin
#include "IProperties.h"

/**********************************************************************************************//**
 * \class	PropertiesWin
 *
 * \brief	Windows implementation of IProperties
 *
 * \author	Friedrich
 * \date	05.09.2015
 **************************************************************************************************/

class PropertiesWin : public IProperties
{
public:
	explicit PropertiesWin(const char* cstrRegPath);
	virtual ~PropertiesWin();
	virtual string getString(const string& key, const string& defaultVal) const;
	virtual bool setString(const string& key, const string& value) const;
protected:
	/** \brief	Full pathname to the registry item. */
	string		_regPath;
};

#endif
