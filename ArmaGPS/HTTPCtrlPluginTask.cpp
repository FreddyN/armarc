/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <string>
#include <json/json.h>
#include "HTTPCtrlPluginTask.h"

using std::string;

const char HTTPCtrlPluginTask::_str_cmd[]	= "cmd";
const char HTTPCtrlPluginTask::_str_OK[]	= "OK";
const char HTTPCtrlPluginTask::_str_ERROR[] = "ERROR";
const char HTTPCtrlPluginTask::_str_start[] = "start";
const char HTTPCtrlPluginTask::_str_stop[]	= "stop";
const char HTTPCtrlPluginTask::_str_status[] = "status";


HTTPCtrlPluginTask::HTTPCtrlPluginTask()
{
}

HTTPCtrlPluginTask::~HTTPCtrlPluginTask()
{
}

void HTTPCtrlPluginTask::run()
{
	Json::Reader	jsr;
	Json::Value		root;	
	if (jsr.parse(getData().params, root))
	{
		if (root[_str_cmd] != Json::nullValue)
		{
			string cmd = root[_str_cmd].asString();

			// start http server
			if (cmd.compare(_str_start) == 0)
			{
				_startArmaHTTPServer();
				getData().result = _str_OK;
			}
			else

			// stop http server
			if (cmd.compare(_str_stop) == 0)
			{
				_stopArmaHTTPServer();
				getData().result = _str_OK;
			}
			else

			// return http server status RUNNING/DOWN
			if (cmd.compare(_str_status) == 0)
			{
				
				if (_isArmaHTTPServerRunning())
					getData().result = "RUNNING";
				else
					getData().result = "DOWN";				
			}
			else
				getData().result = _str_ERROR;
		}
		else
			getData().result = _str_ERROR;
	}
	else
		getData().result = _str_ERROR;
}

void HTTPCtrlPluginTask::_startArmaHTTPServer() const
{

}

void HTTPCtrlPluginTask::_stopArmaHTTPServer() const
{

}

bool HTTPCtrlPluginTask::_isArmaHTTPServerRunning() const
{
	bool serverRunning = false;
	return serverRunning;
}
