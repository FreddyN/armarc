/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_MergeJsonCachePluginTask
#define _H_MergeJsonCachePluginTask
#include "GenericPluginTask.h"

class ILogger;
class MergeJsonCachePluginTask :
	public GenericPluginTask
{
public:
	MergeJsonCachePluginTask(ILogger*);
	virtual ~MergeJsonCachePluginTask();
	virtual void run();

private:
	ILogger*			_pLog;
};
#endif
