/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <sstream>
#include "ILogger.h"
#include "Pipe2ArmaHTTP.h"

using std::string;
using std::stringstream;

#define BUFSIZE  32768
#define WAITFORRECONNECT  20000


bool Pipe2ArmaHTTP(const string& wCommand, string& strReceived, LPCWSTR pipeName, ILogger* pLog )
{
	HANDLE hPipe;
	TCHAR  chBuf[BUFSIZE];
	BOOL   fSuccess = FALSE;
	DWORD  cbRead, cbToWrite, cbWritten, dwMode;
	// Try to open a named pipe; wait for it, if necessary. 
	while (1)
	{
		hPipe = CreateFile(
			pipeName,   // pipe name 
			GENERIC_READ |  // read and write access 
			GENERIC_WRITE,
			0,              // no sharing 
			NULL,           // default security attributes
			OPEN_EXISTING,  // opens existing pipe 
			0,              // default attributes 
			NULL);          // no template file 

							// Break if the pipe handle is valid. 
		if (hPipe != INVALID_HANDLE_VALUE)
			break;
		// Exit if an error other than ERROR_PIPE_BUSY occurs. 
		if (GetLastError() != ERROR_PIPE_BUSY)
		{			
			return false;
		}
		// All pipe instances are busy, so wait for 20 seconds. 
		if (!WaitNamedPipe(pipeName, WAITFORRECONNECT))
		{
			return false;
		}
	}

	// The pipe connected; change to message-read mode. 

	dwMode = PIPE_READMODE_MESSAGE;
	fSuccess = SetNamedPipeHandleState(
		hPipe,    // pipe handle 
		&dwMode,  // new pipe mode 
		NULL,     // don't set maximum bytes 
		NULL);    // don't set maximum time 
	if ((!fSuccess) && (pLog))
	{
		stringstream strErr;
		strErr << "SetNamedPipeHandleState failed. GLE=" << GetLastError();
		pLog->log(ILogger::logLevel::error, strErr.str());
		return false;
	}

	cbToWrite = wCommand.length() * sizeof(wCommand[0]);
	fSuccess = WriteFile(
		hPipe,                  // pipe handle 
		wCommand.c_str(),       // message 
		cbToWrite,              // message length 
		&cbWritten,             // bytes written 
		NULL);                  // not overlapped 

	if ((!fSuccess) && (pLog))
	{
		stringstream strErr;
		strErr << "WriteFile to pipe failed. GLE=" << GetLastError();
		pLog->log(ILogger::logLevel::error, strErr.str());
		return false;
	}
	do
	{
		// Read from the pipe. 
		fSuccess = ReadFile(
			hPipe,    // pipe handle 
			chBuf,    // buffer to receive reply 
			BUFSIZE*sizeof(TCHAR),  // size of buffer 
			&cbRead,  // number of bytes read 
			NULL);    // not overlapped 

		if (!fSuccess && GetLastError() != ERROR_MORE_DATA)
			break;
		const string strResult((const char*)chBuf, cbRead);
		strReceived = strResult;
	} while (!fSuccess);  // repeat loop if ERROR_MORE_DATA 

	if ((!fSuccess) && (pLog))
	{
		stringstream strErr;
		strErr << "ReadFile from pipe failed. GLE=" << GetLastError();
		pLog->log(ILogger::logLevel::error, strErr.str());
		return false;
	}
	CloseHandle(hPipe);
	return true;
}
