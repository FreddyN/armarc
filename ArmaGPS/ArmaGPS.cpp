/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <atomic>
#include <queue>
#include "ILogger.h"

#include "PluginFactory.h"

using std::string;
using std::queue;
using std::unordered_map;
using std::mutex;
using std::to_string;
using std::thread;
using std::pair;
using std::cout;
using std::endl;

static const char cstr_Invalid_Command[] = "INVALID COMMAND";
static const char cstr_Result_Wait[] = "WAIT";
static const char cstr_Result_Empty[] = "EMPTY";

bool httpStarted = false;
unordered_map< long int, GenericPluginTask* > g_tickets;
queue< GenericPluginTask* > g_task_queue;
mutex mtx;

bool worker_working = false;
atomic<bool> worker_running(false);
long int id = 0; // global ticket id
long int cur_id = 0; // current ticket id
extern ILogger* stdlog;

extern PluginFactory g_PluginFactory;


extern "C"
{
	__declspec (dllexport) void __stdcall RVExtension(char *output, int outputSize, const char *function);
}

void worker()
{
	worker_running.exchange(true);
	while ( worker_running.load() )
	{
		GenericPluginTask* pTask = nullptr;
		mtx.lock();		
		bool emptyqueue = true;
		// TODO: Review, wake up with an event, when something has put to the queue, instead of polling the queue
		if (g_task_queue.size())
		{
			pTask = g_task_queue.front();
			g_task_queue.pop();
			if (g_task_queue.size())
				emptyqueue = false;
		}
		mtx.unlock();
		if (pTask)
		{
			pTask->run();
			pTask->getData().ready.exchange(true);
			if (emptyqueue) // Offer tread switch to the scheduler
				Sleep(0);
		}
		else
		{
			// Empty Queue, nothing to do, offer thread switch to the scheduler
			Sleep(0);
		}
	}
}

GenericPluginTask* CreatePluginTaskFromFactoryEx( PluginFactory& factory, const string& param, ILogger* pLog)
{
	size_t functionSeparator = param.find_first_of(" ");
	string functionName;
	string strparam;
	if (functionSeparator != string::npos)
	{
		functionName = param.substr(0, functionSeparator);
		strparam = param.substr(functionSeparator + 1);
	}
	else
		functionName = param;

	GenericPluginTask* task = nullptr;
	if (functionName.compare("EXIT") == 0)
		worker_running.exchange(false);
	else
		task = factory.create(functionName);

	if (task)
		task->getData().params = strparam;
	return task;
}


void __stdcall RVExtension(char *output, int outputSize, const char *function)
{		
	if (!strcmp(function, "version"))
	{		
		strncpy_s(output, outputSize, "1.0", _TRUNCATE); // result		
		return;
	}

	if (!strncmp(function, "r:", 2)) // detect checking for result
	{
		auto num = atol(&function[2]); // ticket number or 0

		if (g_tickets.find(num) != g_tickets.end()) // ticket exists
		{			
			if (g_tickets[num]->getData().ready.load()) // result is ready
			{
				strncpy_s(output, outputSize, g_tickets[num]->getData().result.c_str(), _TRUNCATE); // result
				g_tickets.erase(num); // get rid of the read ticket
				delete g_tickets[num];								
				return;
			}		
			strncpy_s(output, outputSize, cstr_Result_Wait, _TRUNCATE); // result is not ready
			return;
		}
		strncpy_s(output, outputSize, cstr_Result_Empty, _TRUNCATE); // no such ticket
	}
	else if (!strncmp(function, "s:", 2)) // detect ticket submission
	{
		const string& funcName = &function[2];
		auto newTask = CreatePluginTaskFromFactoryEx(g_PluginFactory, funcName, stdlog );
		if (newTask)
		{
			g_tickets.insert(pair<long int, GenericPluginTask*>(++id, newTask)); // add ticket to the queue
			mtx.lock();
			g_task_queue.push(newTask);
			mtx.unlock();

			if (!worker_working) // if worker thread is finished, start another one
			{
				worker_working = true;
				thread worker1(worker);
				worker1.detach(); // start parallel process
			}
			strncpy_s(output, outputSize, to_string(id).c_str(), _TRUNCATE); // ticket number
		}
		else strncpy_s(output, outputSize, cstr_Invalid_Command, _TRUNCATE); // Factory does not support this function
	}
	else if (!strncmp(function, "d:", 2)) // detect direct call
	{
		const string& funcName = &function[2];
		auto newTask = CreatePluginTaskFromFactoryEx(g_PluginFactory, funcName, stdlog);
		if (newTask)
		{
			newTask->run();
			const auto len = newTask->getData().result.length();
			if (static_cast<size_t>(outputSize) > len )
				strncpy_s(output, outputSize, newTask->getData().result.c_str(), _TRUNCATE);
			else
				output[0] = 0;
			delete newTask;
		}
		else strncpy_s(output, outputSize, cstr_Invalid_Command, _TRUNCATE); // other input	
	}
	else strncpy_s(output, outputSize, cstr_Invalid_Command, _TRUNCATE); // other input			
}
