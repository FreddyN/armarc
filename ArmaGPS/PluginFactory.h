/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef _H_PluginFactory
#define _H_PluginFactory
#include <map>
#include "TFactory.h"
#include "GenericPluginTask.h"

using std::map;

/**********************************************************************************************//**
 * \class	PluginFactory
 *
 * \brief	A plugin factory for PluginTasks
 *
 * \author	Friedrich
 * \date	05.09.2015
 **************************************************************************************************/

class PluginFactory : public TFactory<GenericPluginTask>
{
public:
	PluginFactory();
	PluginFactory(const PluginFactory& src);
	virtual ~PluginFactory();
	virtual GenericPluginTask* create(const string& cstrName) const;
	virtual bool addProducer(const string& cstrName, function<GenericPluginTask* ()>);
	virtual function<GenericPluginTask*()> getProducer(const string& cstrName) const;
	virtual bool removeProducer(const string& cstrName);
	virtual void getProducerNames(set<string>& producerNames) const;
	virtual void extend(const TFactory<GenericPluginTask>& src);
protected:
	map<string, function<GenericPluginTask* () > > _producers;
};

#endif

