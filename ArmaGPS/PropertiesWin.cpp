/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdafx.h>
#include <Windows.h>
#include "PropertiesWin.h"

using std::wstring;
using std::wstring;

/** \brief	The arma GPS properties. */
static PropertiesWin g_ArmaGPSProperties("SOFTWARE\\Banana Squad\\ArmaRC");

/**********************************************************************************************//**
 * \fn	IProperties& getSysProperties()
 *
 * \brief	Gets system properties.
 *
 * \author	Friedrich
 * \date	05.09.2015
 *
 * \return	The system properties.
 **************************************************************************************************/

IProperties& getSysProperties()
{
	return g_ArmaGPSProperties;
}

/**********************************************************************************************//**
 * \fn	PropertiesWin::PropertiesWin(const char* cstrRegPath)
 *
 * \brief	Constructor.
 *
 * \author	Friedrich
 * \date	05.09.2015
 *
 * \param	cstrRegPath	Full pathname of the cstr register file.
 **************************************************************************************************/

PropertiesWin::PropertiesWin(const char* cstrRegPath) :
	_regPath(cstrRegPath)
{
}

/**********************************************************************************************//**
 * \fn	PropertiesWin::~PropertiesWin()
 *
 * \brief	Destructor.
 *
 * \author	Friedrich
 * \date	05.09.2015
 **************************************************************************************************/

PropertiesWin::~PropertiesWin()
{
}

/**********************************************************************************************//**
 * \fn	string PropertiesWin::getString(const string& key, const string& default) const
 *
 * \brief	Gets a string.
 *
 * \author	Friedrich
 * \date	05.09.2015
 *
 * \param	key	   	The key.
 * \param	default	The default.
 *
 * \return	The string.
 **************************************************************************************************/

string PropertiesWin::getString(const string& key, const string& defaultVal) const
{
	HKEY hKey;
	const wstring strPath(_regPath.begin(), _regPath.end());
	auto lRes = RegOpenKeyExW(HKEY_LOCAL_MACHINE, strPath.c_str(), 0, KEY_READ, &hKey);
	auto bExistsAndSuccess(lRes == ERROR_SUCCESS);
	if (!bExistsAndSuccess)
		return defaultVal;

	WCHAR szBuffer[1024];
	DWORD dwBufferSize = sizeof(szBuffer);
	ULONG nError;
	ZeroMemory(szBuffer, dwBufferSize);
	const wstring strwKey(key.begin(), key.end());
	nError = RegQueryValueEx(hKey, strwKey.c_str(), 0, NULL, (LPBYTE) szBuffer, &dwBufferSize);
	if (ERROR_SUCCESS == nError)
	{
		wstring strwResult = szBuffer;
		return string(strwResult.begin(), strwResult.end());		
	}
	else
		return defaultVal;
}

/**********************************************************************************************//**
 * \fn	bool PropertiesWin::setString(const string& key, const string& value) const
 *
 * \brief	Sets a string.
 *
 * \author	Friedrich
 * \date	05.09.2015
 *
 * \param	key  	The key.
 * \param	value	The value.
 *
 * \return	true if it succeeds, false if it fails.
 **************************************************************************************************/

bool PropertiesWin::setString(const string& key, const string& value) const
{
	return false; // Dummy implementation
}


