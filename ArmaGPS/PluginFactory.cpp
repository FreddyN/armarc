/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <stddef.h>
#include "PluginFactory.h"

PluginFactory::PluginFactory()
{

};

PluginFactory::~PluginFactory()
{

};

PluginFactory::PluginFactory(const PluginFactory& src)
{
	_producers = src._producers;
}

bool PluginFactory::addProducer(const string& cstrName, function<GenericPluginTask* ()> func)
{
	if (_producers.find(cstrName) == _producers.end())
	{
		_producers.insert(std::pair<string, function<GenericPluginTask* ()> >(cstrName, func));
		return true;
	}
	else
		return false;
};

GenericPluginTask* PluginFactory::create(const string& cstrName) const
{
	const map<string, function<GenericPluginTask* ()> >::const_iterator pProducer 
		= _producers.find(cstrName);
	if (pProducer != _producers.end())
		return pProducer->second();	
	return (GenericPluginTask*) nullptr;
}

function<GenericPluginTask*()> PluginFactory::getProducer(const string& cstrName) const
{
	const map<string, function<GenericPluginTask* ()> >::const_iterator pProducer
		= _producers.find(cstrName);
	if (pProducer == _producers.end())
		throw std::runtime_error("Unknown producer");
	return pProducer->second;
}

void PluginFactory::getProducerNames(set<string>& producerNames) const
{
	producerNames.clear();
	for (auto producer : _producers)
		producerNames.insert(producer.first);
	return;
}

void PluginFactory::extend(const TFactory<GenericPluginTask>& src)
{
	set<string> srcProducerNames;
	src.getProducerNames(srcProducerNames);
	for (auto producerName : srcProducerNames)
		addProducer(producerName, src.getProducer(producerName));
}

bool PluginFactory::removeProducer(const string& cstrName)
{
	const map<string, function<GenericPluginTask* ()> >::const_iterator pProducer
		= _producers.find(cstrName);
	if (pProducer != _producers.end())
	{
		_producers.erase(pProducer);
		return true;
	}
	return false;
}