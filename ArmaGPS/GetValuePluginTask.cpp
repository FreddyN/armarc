/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <conio.h>
#include <tchar.h>

#include <Json/Json.h>
#include "GetValuePluginTask.h"
#include "ModuleLoggerDecorator.h"
#include "Pipe2ArmaHTTP.h"

using std::string;

GetValuePluginTask::GetValuePluginTask(ILogger* pLog) :
	_pLog(pLog)
{
}

GetValuePluginTask::~GetValuePluginTask()
{

}

void GetValuePluginTask::run()
{	
	string wCommand = "GetValue ";
	wCommand.append(getData().params);
	if (!Pipe2ArmaHTTP(wCommand, getData().result))
		if (_pLog)
			ModuleLoggerDecorator(__func__, _pLog).log(ILogger::logLevel::error, "Pipe2ArmaHTTP Failed.");	
}
