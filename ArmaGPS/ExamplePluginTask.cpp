/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include <iostream>
#include "ExamplePluginTask.h"

using std::cout;
using std::endl;

ExamplePluginTask::ExamplePluginTask( ILogger* pLog):
	_pLog( pLog )
{	
}

void ExamplePluginTask::run()
{
	cout << "Example function with " << getData().params << endl;
	// Do some stuff
	Sleep(( rand() * 1000) / RAND_MAX);
	getData().result = "OK";
}

ExamplePluginTask::~ExamplePluginTask()
{
}
