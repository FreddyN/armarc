/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <tchar.h>
#include <string>
#include <sstream>
#include <iostream>
#include "RVTestCalls.h"
#include <gtest/gtest.h>

using std::stringstream;
using std::cout;
using std::endl;

int testcases(int argc, _TCHAR* argv[])
{
	
	// Asynchron Plugin calls.
	cout << "Make some action; keep the plugin busy with async calls....";
	for (auto i = 0; i < 2500; i++)
	{
		stringstream strPluginCall;
		strPluginCall << "WriteJsonCache AllPlayers "
			<< "{ \"TestPlayer\" : \"Freddy\", \"Score\" : " << i << "}";
		TestAsyncCallWithTickets(strPluginCall.str().c_str());		
	}
	cout << "done." << std::endl;

	// Synchron Plugin calls.
	cout << "Make some action; keep the plugin busy with synchronized calls....";
	for (auto i = 0; i < 2500; i++)
	{
		stringstream strPluginCall;
		strPluginCall << "WriteJsonCache AllPlayers "
			<< "{ \"TestPlayer\" : \"Freddy\", \"Score\" : " << i << "}";
		TestSyncCall(strPluginCall.str().c_str());
	}
	cout << "done." << endl;
	cout << "Timeout for 60 sec. for additional test on http request, before shutdown http server" << endl;
	return 0;
}

GTEST_API_ int main(int argc, char** argv)
{
	testing::InitGoogleTest(&argc, argv);
	auto errors = RUN_ALL_TESTS();
	if (!errors)
	{
		cout << "Test successful. Starting explorer..." << endl;
		system("start http://localhost:9192/index.html");
	}
}
