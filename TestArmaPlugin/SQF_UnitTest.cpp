/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtest/gtest.h>
#include "RVTestCalls.h"

TEST(SQF, InvalidCommandResponse)
{
	std::string resultWrite = TestSyncCall("Foo ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("INVALID COMMAND"), 0);
}

TEST(SQF,WriteAndMergeJsonCache)
{
	std::string resultWrite  = TestSyncCall("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultMerge = TestSyncCall("MergeJsonCache ObjectName { \"cmd2\" : \"status2\" }");	
	EXPECT_EQ(resultWrite.compare("OK"), 0);
}

TEST(SQF, WriteAndMergeJsonCacheAsync)
{
	std::string resultWrite = TestAsyncCallWithTickets("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultMerge = TestAsyncCallWithTickets("MergeJsonCache ObjectName { \"cmd2\" : \"status2\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);
}

TEST(SQF, getValue_Testcase1)
{
	std::string resultWrite = TestSyncCall("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);
	
	std::string resultGet = TestSyncCall("GetValue ObjectName cmd1");
	EXPECT_EQ(resultGet.compare("[\"status1\"]"), 0);
}

TEST(SQF, getValue_Testcase1async)
{
	std::string resultWrite = TestAsyncCallWithTickets("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultGet = TestSyncCall("GetValue ObjectName cmd1");
	EXPECT_EQ(resultGet.compare("[\"status1\"]"), 0);
}

TEST(SQF, getValue_Testcase2)
{
	static std::string	testJson1 = "{ \"Object1\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" , \"Object2\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" } } }";
	std::string resultWrite = TestSyncCall("WriteJsonCache ObjectName " + testJson1);
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultGet = TestSyncCall("GetValue ObjectName Object1/Key1");
	EXPECT_EQ(resultGet.compare("[\"Value1\"]"), 0);
}

TEST(SQF, getValue_Testcase2async)
{
	static std::string	testJson1 = "{ \"Object1\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" , \"Object2\" : { \"Key1\" : \"Value1\", \"Key2\" : \"Value2\" } } }";
	std::string resultWrite = TestAsyncCallWithTickets("WriteJsonCache ObjectName " + testJson1);
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultGet = TestAsyncCallWithTickets("GetValue ObjectName Object1/Key1");
	EXPECT_EQ(resultGet.compare("[\"Value1\"]"), 0);
}

TEST(SQF, fetchValue_Testcase1)
{
	std::string resultWrite = TestSyncCall("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultGet1 = TestSyncCall("FetchValue ObjectName cmd1");
	EXPECT_EQ(resultGet1.compare("[\"status1\"]"), 0);

	// cmd1 is now removed
	std::string resultGet2 = TestSyncCall("FetchValue ObjectName cmd1");
	EXPECT_EQ(resultGet2.compare("ERROR"), 0);
}

TEST(SQF, fetchValue_Testcase1async)
{
	std::string resultWrite = TestAsyncCallWithTickets("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultGet1 = TestAsyncCallWithTickets("FetchValue ObjectName cmd1");
	EXPECT_EQ(resultGet1.compare("[\"status1\"]"), 0);

	// cmd1 is now removed
	std::string resultGet2 = TestAsyncCallWithTickets("FetchValue ObjectName cmd1");
	EXPECT_EQ(resultGet2.compare("ERROR"), 0);
}

TEST(SQF, fetchValue_Testcase2)
{
	std::string resultWrite = TestSyncCall("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultGet1 = TestSyncCall("FetchValue ObjectName cmd2/status1");
	EXPECT_EQ(resultGet1.compare("ERROR"), 0);
}

TEST(SQF, fetchValue_Testcase2async)
{
	std::string resultWrite = TestAsyncCallWithTickets("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultGet1 = TestAsyncCallWithTickets("FetchValue ObjectName cmd2/status1");
	EXPECT_EQ(resultGet1.compare("ERROR"), 0);
}

TEST(SQF, fetchValue_Testcase_NotExistingObject)
{
	std::string resultGet1 = TestSyncCall("FetchValue NotExistingObject cmd2/status1");
	EXPECT_EQ(resultGet1.compare("ERROR"), 0);
}

TEST(SQF, fetchValue_Testcase_NotExistingPath)
{
	std::string resultWrite = TestSyncCall("WriteJsonCache ObjectName { \"cmd1\" : \"status1\" }");
	EXPECT_EQ(resultWrite.compare("OK"), 0);

	std::string resultGet1 = TestSyncCall("FetchValue OjbectName cmd1/status1");
	EXPECT_EQ(resultGet1.compare("ERROR"), 0);
}

TEST(SQF, HTTPSecured_ArmaRC_Configuration)
{
	std::string result_armacurl = TestSyncCall("GetValue ArmaRC armarcurl");
	ASSERT_GT(result_armacurl.length(), (size_t) 5);

	std::string result_httpprotected = TestSyncCall("GetValue ArmaRC httpprotected");
	EXPECT_GT(result_httpprotected.length(), (size_t) 5);
}
