/*
Copyright(C) 2015, Friedrich Nowak
This program is free software : you can redistribute it and / or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <windows.h>
#include "RVTestCalls.h"
#ifdef _DEBUG
#include <iostream>
#endif

extern "C"
{
	__declspec (dllimport) void __stdcall RVExtension(char *output, int outputSize, const char *function);
}

string TestAsyncCallWithTickets(const string& strFunction)
{
	char strTicket[80];
	char strResult[80];
	string strFuncCall = "s:";
	strFuncCall.append(strFunction);

	// Asynchron Arma3 call, get Ticket
	RVExtension(strTicket, sizeof(strTicket) / sizeof(strTicket[0]), strFuncCall.c_str());

	// Waiting for plugin ticket result
	string Ticket = "r:";
	Ticket.append(strTicket);
#ifdef _DEBUGOUT
	std::cout << "Waiting for results ";
#endif
	do
	{
		RVExtension(strResult, sizeof(strResult) / sizeof(strResult[0]), Ticket.c_str());
		if (string(strResult).compare("WAIT") == 0)
		{
#ifdef _DEBUGOUT
			std::cout << ".";
#endif _DEBUGOUT
			Sleep(1);
		}
#ifdef _DEBUGOUT
		else
		{
			std::cout << std::endl;
			std::cout << "Result:" << strResult << std::endl;
		}
#endif
	} while (string(strResult).compare("WAIT") == 0);
	return string(strResult);
}

string TestSyncCall(const string& strFunction)
{
	char strResult[80];
	string strFuncCall = "d:";
	strFuncCall.append(strFunction);

	RVExtension(strResult, sizeof(strResult) / sizeof(strResult[0]), strFuncCall.c_str());

#ifdef _DEBUGOUT
	std::cout << "Result:" << strResult << std::endl;
#endif
	return string(strResult);
}
